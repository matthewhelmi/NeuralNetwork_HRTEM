import os
import glob
import argparse

import numpy as np
#import cupy as cp

import matplotlib.pyplot as plt
import tensorflow.keras as keras

from temnn.analysis.ExperimentalAnalysis import Experimental_Data, Fourier_Transform
from datetime import datetime

from skimage.feature import peak_local_max, blob_log
from skimage.segmentation import watershed
from scipy import ndimage as ndi

from cupyx.scipy import ndimage as cp_ndi

from sklearn.metrics import accuracy_score, f1_score

def expand_mask(mask, radius=20):
    expand = mask.copy()
    for j in range(radius,mask.shape[0]):
        for k in range(radius,mask.shape[1]):
            window = mask[j-radius:j+radius,k-radius:k+radius]
            fraction = np.count_nonzero(window == 1.0)/(window.shape[0]*window.shape[1])
            if fraction > 0.1:
                expand[j,k] = 1.0
    return expand

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    positionalparams = ('mask_segmentation', 'column_segmentation', 'datf', 'outf')
    parser.add_argument("mask_segmentation",
            help="Network for mask segmentation.")
    #parser.add_argument("column_segmentation",
    #        help="Nework for column segmentation.")
    parser.add_argument("datf",
            help="The path and name of the folder where the training data is placed.")
    parser.add_argument('--save_frames', action='store_true')
    parser.add_argument("--crop", default=None, nargs='+', type=int,
                        help="Crop (x,y)")
    parser.set_defaults(save_frames=False)
    args = parser.parse_args()
    
    if args.crop is not None:
        crop = tuple(args.crop)
    else:
        crop = None
    
    
    folder = args.datf 
    files=glob.glob(f'{folder}/Hour_*/Minute_*/Second_*/*.dm4')

    # Network for nanoparticle segmentation
    nnf = args.mask_segmentation 
    mod = keras.models.load_model(nnf)
    print(f'Using {nnf.split("/")[-2:]} for nanoparticle mask segmentation.')
    
    output_dir='../experimental_data/NoiseAnalysis/{}'.format(nnf.split('/')[-2]+'_'+nnf.split('/')[-1]+'_simple_debug')
    print('Output: '+output_dir)
    #output_dir=folder.replace('wibang', 'wibang_out/NoisePerformance/{}'.format(nnf.split('/')[-2]+'_'+nnf.split('/')[-1]+'_simple_debug'))
    # Check if an output folder exists first, if not then creates one (will only create one the first time for a new dataset)
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    
    t = datetime.now().strftime("%Y%m%d-%H%M%S")
    signal_file = open(f'{output_dir:s}/{t:s}-signal.log', 'at',
                       buffering=1)
    line = ''
    for m in ['doserate', 'area',  'mask_f1', 'mask_relative_score']:
        line = line + f'{m:<20s}'
    print(line.strip(), file=signal_file, flush=True)
   
    ## Segment last frame for mask
    exp_dataset = Experimental_Data(files[-1])
    image = exp_dataset.load(crop=crop)
    image = exp_dataset.cp_local_standardise()
 
    mask_groundtruth = exp_dataset.infer(mod, smooth=None, threshold=0.9)

    for i,f in enumerate(files):
        print(i, f)
        ## Prepare image
        exp_dataset = Experimental_Data(f)
        image = exp_dataset.load(crop=crop)
        image = exp_dataset.cp_local_standardise()
        doserate = exp_dataset.compute_dose_rate(
                                corner=(0, 
                                exp_dataset.image.shape[1]-int(1/exp_dataset.y_cal)
                                  ))
        
        ## Nanopaticle mask segmentation
        mask_prediction = exp_dataset.infer(mod, smooth=None, threshold=0.9)
        if args.save_frames:
           exp_dataset.plot()
           plt.savefig(f'{output_dir:s}/frame_{i:3d}.png')
           plt.close()

        area = np.sum(mask_prediction[0,:,:,0])*(exp_dataset.x_cal)*(exp_dataset.y_cal)
        mask_f1 = f1_score(mask_prediction[0,:,:,0].flatten(), mask_groundtruth[0,:,:,0].flatten())
        mask_rel_score = np.sum(mask_prediction[0,:,:,0]) / np.sum(mask_groundtruth[0,:,:,0])
     
        ## Output values
        line = ''
        for m in [doserate, area, mask_f1, mask_rel_score]:
            line = line + f'{m:.3f} '
        print(line.strip(), file=signal_file, flush=True)
    signal_file.close()
