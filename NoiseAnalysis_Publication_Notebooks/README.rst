Reproducing figures from the Noise Analysis publication
===============================================

https://doi.org/10.48550/arXiv.2302.12629

Figures in the manuscript are produced by these notebooks. Instructions on how to reproduce the data sets appear below.


Creators and contributors:
===========================

(Name, ORCID)

* Matthew Helmi Leth Larsen, 0000-0002-2415-9388
* William Bang Lomholdt, 0000-0002-9993-0675
* Cuauhtemoc Nunez Valencia, 0000-0002-9073-888X
* Thomas W. Hansen, 0000-0002-1861-3054
* Jakob Schiotz, 0000-0002-0670-8013


Reproducing the data
====================

The data required to reproduce the data in the publication is
available from doi:10.11583/DTU.22836338

Download the file and unpack it.  It contains:

* A copy of these instructions (README.md)
* A folder ``scripts`` with scripts for reproducing the data
* A folder ``trained_models`` with the trained models
* A folder ``simulation_data`` with the simulated training and test data sets
* A folder ``experimental_data`` with the experimental frames.

The code can be downloaded by cloning the gitlab repository at
https://gitlab.com/matthewhelmi/NeuralNetwork_HRTEM:

    git clone https://gitlab.com/matthewhelmi/NeuralNetwork_HRTEM.git
    cd NeuralNetwork_HRTEM

To use it, you need a Python installation with TensorFlow 2.5,
ASE 3.22 and abTEM.  The version of ASE and abTEM should be
uncritical, TensorFlow may cause trouble if you use a later version,
as they tend to change the API occationally.  See
https://gitlab.com/schiotz/NeuralNetwork_HRTEM for installation
instructions.


Reproducing the Au_fcc_CeO2_MTF1_22112022 data set
----------------------------------------------------------------------

1. Reproducing the structures

   The folder ``simulation_data/Au_fcc_CeO2_MTF1_22112022`` contains a script
   ``reproduce_supported.sh``.  Copy it to the folder
   ``NeuralNetwork_HRTEM/workflow/simulate_waves/simulate_2Dmaterials``.

   The file contains the python command that reproduces the structures
   for the unsupported MOS2 training data set.  The python script is
   called with a number of arguments aimed at producing exactly the
   same result, including a ``--seed`` argument seeding the random
   number generator, and a ``--numproc`` argument setting the number
   of parallel processes used when generating structures.  To
   reproduce the same structures, neither must be changed.  If your
   computer has more or less than 24 cores, the script will over- or
   undersubscribe your CPU cores, that may hurt performance a bit but
   is otherwise not problematic.  The output will appear in the folder
   ``NeuralNetwork_HRTEM/workflow/simulation_data/Au_fcc_CeO2_MTF1_22112022``. 

   Do the same with the similar script in
   ``simulation_data/Au_fcc_CeO2_MTF1_22112022-test`` to produce the test data.

2. Reproducing the images

   The folder ``simulation_data/Au_fcc_CeO2_MTF1_22112022`` contains a file
   ``parameters.json`` with the microscope paramters.  Copy it to the
   folder ``NeuralNetwork_HRTEM/workflow/simulate_images`` and name it
   ``parameters_train.json``.  In that
   folder, run the following script (or submit it to a computer
   cluster)

       python make_image_data.py ../simulation_data/Au_fcc_CeO2_MTF1_2211202 parameters_train.json --train

   Note that this will run on all available cores.  You can specify
   the number of cores with ``-n 10``, for example.  Unlike the script
   that generates the structures, the result will not depend on the
   number of cores used, as all random numbers are generated on the
   master.  The seed for the random number generator is in the json
   file.

   You now need to copy the similar ``parameters.json`` file from
   ``simulation_data/Au_fcc_CeO2_MTF1_22112022-test``, name it ``parameters_test.json`` and run
   the command

       python make_image_data.py ../simulation_data/Au_fcc_CeO2_MTF1_22112022-test parameters_test.json --test

   It is unfortunate that the random number seed was different in when
   producing the training and test set, as that makes it necessary to
   use different json files (the script was made to generate different
   sequences from the same seed depending on whether ``--train`` or
   ``--test`` was specified).

4. Train the networks

   The folder ``trained_networks/MSDnet_Au_fcc_CeO2_MTF1_22112022`` contains a script
   ``reproduce_train.sh``.  Copy it to the folder
   ``NeuralNetwork_HRTEM/workflow/train_simulation/MSDnet``.
   
   The file contains the python command that reproduces the training procedure
   of an MSDnet on the MTF1 training data set.  The python script is
   called with a number of arguments aimed at reproducing a training procedure
   with the same set up, such as the number of layers and training epochs, including
   a ``--filters`` argument setting the number of output channels for each convolutional 
   layer, and a ``--kernel`` argument setting the size of the kernel used. The output
   will appear in the folder ``NeuralNetwork_HRTEM/workflow/MSDnet``.

   The training requires a GPU.
   
   The same can be done for all data sets and for the Unet.

   **Alternative:** Find the trained network in the supplied
   ``trained_networks`` folder.
   
5. Analyse Experimental Data

   Inspect and xecute ``analyse_experimental.sh`` which will store the area of segmentation by the
   specified trained model and store the output as .log files in the
   ``experimental_data/Noise_analysis`` folder.
   
   A GPU will be required and this will be take some time due to the number of images.
   
   **Alternative:** Find the .log files in the existing ``experimental_data/Noise_analysis``
   folder.
   

Reproducing the remaining data sets
-----------------------------------------------

Follow the same instructions as above, but find the corresponding data set files in the
``simulation_data`` folder.


Extra notes
==========


Figure 1
-----------

This figure requires the atomic models and images of the ``Au_fcc_CeO2_MTF1_22112022``
data set. Run steps 1 and 2 to obtain these and modify the paths in the ``Figure1-Visual.ipynb``.

Additionally POV-Ray will be required to render the atomic structures, from the .ini files
generated in ``Figure1-Visual.ipynb``. After generating the .ini file from the notebook
execute 

    module load POV-Ray
    povray AuCeO2_0x0y0z.ini

to generate a .png that will be loaded in the same notebook.


Figures 2, 3, 4, 7, 8
------------------------

If you ran ``analyse_experimental.sh`` yourself the .log files are located in eg.

``../experimental_data/Noise_analysis/MSDnet_Au_fcc_CeO2_MTF4_22112022_model-0_simple_debug/``

instead of eg.

``../experimental_data/NoiseAnalysis/MSDnet_Au_fcc_CeO2_MTF4_22112022_model-0_simple_debug/20220301_MEMS13_continous_doserate_increment/ROI3/Au-CeO2-contdoseseries_noADCsmaller/``


Figure SOI5
---------------

This figure requires the images of the ``Au_fcc_CeO2_MTF4_22112022`` data set.
Run steps 1 and 2 to obtain these and modify the paths in ``FigureSOI5-TrainingSize.ipynb``.


Figure SOI9
---------------

A data set of images were generated with a dose range matching the experimental data set
using ``Continuous_dose_series.ipynb``. Regrettably, this wasn't done in a reproducible manner
like the other data sets. The images and parameters are therefore provided in 
``simulation_data/continuous_dose_series`` to reproduce figure SOI 9 with ``FigureSOI9.ipynb``.


How to cite this data:
=======================

doi:10.11583/DTU.22836338


Funding:
=========

The authors acknowledge financial support from the Independent Research Fund Denmark (DFF-FTP)
through grant no. 9041-00161B.
