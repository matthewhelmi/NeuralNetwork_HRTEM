#!/bin/bash -ex
# #SBATCH --job-name=Noise_performance
# #SBATCH --mail-type=START,END
# #SBATCH --partition=sm3090
# #SBATCH --output=slurm-%x-%j.out
# #SBATCH --time=7-00:00:00
# #SBATCH --gres=gpu:RTX3090:1
# #SBATCH -N 1                      # Minimum of 1 node
# #SBATCH -n 8                      # 7 MPI processes per node

#source ~/venv/bin/activate # use if necessary
source /scratch/mhlla/venv/bin/activate

PRE=..

python Noise_analysis.py $PRE/trained_networks/MSDnet_Au_fcc_CeO2_MTF1_22112022/model-0 $PRE/experimental_data/Au-CeO2-contdoseseries_noADCsmaller --crop 608 640

#python Noise_analysis.py $PRE/trained_networks/MSDnet_Au_fcc_CeO2_MTF2_22112022/model-0 $PRE/experimental_data/Au-CeO2-contdoseseries_noADCsmaller --crop 608 640
#
#python Noise_analysis.py $PRE/trained_networks/MSDnet_Au_fcc_CeO2_MTF3_22112022/model-0 $PRE/experimental_data/Au-CeO2-contdoseseries_noADCsmaller --crop 608 640
#
#python Noise_analysis.py $PRE/trained_networks/MSDnet_Au_fcc_CeO2_MTF4_22112022/model-0 $PRE/experimental_data/Au-CeO2-contdoseseries_noADCsmaller --crop 608 640
#
#python Noise_analysis.py $PRE/trained_networks/MSDnet_Au_fcc_CeO2_MTF4_lowdose_22112022/model-0 $PRE/experimental_data/Au-CeO2-contdoseseries_noADCsmaller --crop 608 640
#
#python Noise_analysis.py $PRE/trained_networks/MSDnet_Au_fcc_CeO2_MTF5_22112022/model-0 $PRE/experimental_data/Au-CeO2-contdoseseries_noADCsmaller --crop 608 640
#
#python Noise_analysis.py $PRE/trained_networks/Unet_Au_fcc_CeO2_MTF4_22112022/model-0 $PRE/experimental_data/Au-CeO2-contdoseseries_noADCsmaller --crop 608 640
#
#python Noise_analysis.py $PRE/trained_networks/Unet_Au_fcc_CeO2_MTF4_lowdose_22112022/model-0 $PRE/experimental_data/Au-CeO2-contdoseseries_noADCsmaller --crop 608 640
