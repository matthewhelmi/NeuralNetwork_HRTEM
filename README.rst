===================================================
Deep Learning Assisted Atomic-Resolution Microscopy
===================================================

Table of contents
=================
* General info
* Technologies
* Installation
* Workflow
* Folder Structure
* Credits & further info

General info
============
This is a collection of Python modules, scripts and Jupyter notebooks for using Convolutional Neural Networks to identify atoms, atomic columns and possibly other atomic-scale structures in High Resolution Transmission Electron Microscopy (HRTEM) images.
	
The neural network can be trained on simulated TEM images and subsequently
used on experimental HRTEM images (and image sequences) - this is
particularly useful for analysing large amounts of images and long sequences.

---------------
What can it do?
---------------

* We have trained networks on Gold nanoparticles and used the network to
  track surface diffusion of gold nanoparticles on an oxide support
  under the influence of small amounts of gasses.

* We have also trained a network on graphene-like sheets, and used the
  network to find atoms in defected sheets of graphene, and thus
  calculate the strain field in the graphene.

These methods are described in [JAMAD1]_

* Finally, applying a focal series, the networks are trained to identify
  individual atomic species in industrial-style molybdenum disulfide.


Technologies
============
Project is created with:

* Python version: 3.8
* Tensorflow version: 2.5

Installation
============

This is not a software package to be installed, but a collection of
scripts to be used.  However, some prerequisites should be installed
and some packages should be made available for Python.

To use this software, you need to install the following packages with
pip.  We recommend creating a `virtual environment` (venv) to prevent
interference between your project.

You first need to install Tensorflow.  This may require a specific
version of CUDA and cuDNN installed on your machine to run using
GPUs.  Please refer to the TensorFlow page, as that will depend on the
version of TensorFlow you install.  Tensorflow itself is installed
with::

  python -m pip install tensorflow

You will also need ASE and abTEM.  Installing abTEM will pull in the
necessary prerequisites (including ASE)::

  python -m pip install abTEM
  
.. _`virtual environment`: https://towardsdatascience.com/getting-started-with-python-virtual-environments-252a6bd2240


**Checking out this package**

You now need to clone this package with git (on Windows you may need
to use the Anaconda prompt, and perhaps ``conda install git``)::

  git clone git@gitlab.com:schiotz/NeuralNetwork_HRTEM.git

This will create the folder ``NeuralNetwork_HRTEM``, you need to add
the absolute path to that folder to your ``PYTHONPATH``.


**Installing with Anaconda**

*We do not use Anaconda ourselves, so these instructions may not be up
to date.*

You should install Tensorflow following the instructions on their web
pages, and abTEM using their instructions.  The Tensorflow
instructions involve installing prerequisites using ``conda`` and
Tensorflow using pip.

* `Tensorflow installation instructions <https://www.tensorflow.org/install/pip>`_

* `abTEM installation instructions <https://github.com/jacobjma/abTEM#installation>`_

Then check out this package as described above.

Workflow
========

A general workflow has been designed that goes as follows:

* Simulate your material of interest. This first step generates the exit waves and spatial points 
  of the atoms, which are used to generate the HRTEM images. Here you should generate a dataset 
  for training the neural network and a separate dataset for validating (testing) the neural network.

* Train the neural network on the simulated training data generated in step 1.

* Validate (test) the neural network on your simulated training data and simulated validation (test) 
  data also generated in step 1.

* Finaly test the neural network on a set of experimentally obtained HRTEM images.

This general workflow has been split into two underlying workflows, where the difference lies in
the generation of images. The images can either be generated 'on the fly' or be 'precomputed'.

----------
On the fly
----------
Images are generated on the fly (otf). This means that while training, HRTEM images are generated from the
exit waves using a random selection of microscope parameters in a specified range. All images are not stored
for later use, thus during validation, images are again generated based on the exit waves and the same range
of parameters. The only data that is reused are the exit waves and parameter ranges. The same exit wave with
different microscope parameters, produces a different image.

* Key advantage: The neural network never sees the exact same image twice.

-----------
Precomputed
-----------
A set of images are generated before training and validating. This follows a more classic machine/deep 
learning workflow. After the exit waves are produced, a set of HRTEM images are generated based on the exit waves 
and random values within a specified range of microscope parameters. These images are saved in folders respective 
to the material. The neural network is then trained on the training set of images, and validated on the validation 
set of images.

* Key advantage: Training and validation is significantly faster. Training data can be reused for reproducibility.


Folder Structure
================

workflow
  Folders containing the workflow. This is where simulating, training, validating, and testing happens.
 
temnn
  Folders containing the software. This includes implemntations of neural networks, classes for generating simulated
  HRTEM images, data structures for handling the HRTEM images, scripts for calculating precision and recall, and more. 

stm
  Helper files taken from Jacob Madsen's structural-template-matching 
  project at https://github.com/jacobjma/structural-template-matching.


------------------
HOW TO / Tutorials
------------------

Tutorials / HOWTOs will appear here as they are written.

* `HOW TO get this project downloaded to my computer <HOWTO/HOWTO_gitlab.rst>`_.
  
* `HOW TO analyze nanoparticle image sequences <HOWTO/HOWTO_nanoparticle_sequences.rst>`_.

* `HOW TO run on the DTU Compute GPU cluster <HOWTO/HOWTO_DTU_Compute.rst>`_.

Credits & further info
======================
This code is based on Jakob Schiotz' code at
https://github.com/schiotz/NeuralNetwork_HRTEM.  Major parts
of the code originally belong to Jacob Madsen at  
https://github.com/jacobjma/atomic-resolution-tensorflow, 
but relevent scripts have been reimplemented with
`Tensorflow 2.0 <http://tensorflow.org>`_, including integrated CPU and GPU support
and `tf.keras <https://www.tensorflow.org/guide/keras>`_.  Keras provides
a high-level interface and sensible defaults for most parameters.

-------------------------------
Microscopy & Microanalysis 2018
-------------------------------

A great deal of this work was presented at the `M&M 2018 conference
<https://www.microscopy.org/mandm/2018/>`_

A snapshot of the code at the time of presentation, as well as Jupyter
Notebooks creating many of the figures for the slides are available in
the ``M_and_M_2018`` folder in this project, see
`M_and_M_2018/README.rst <OLD/M_and_M_2018/README.rst>`_.

A PDF of my presentation (without the movies and animations) is
available as well: `M_and_M_2018/MandM18_Schiotz.pdf <OLD/M_and_M_2018/MandM18_Schiotz.pdf>`_


---------------------
License and copyright
---------------------

This work is made available under the MIT License.

Copyright 2022 The Technical University of Denmark

Authors:

* Matthew Helmi Leth Larsen
* Jakob Schiøtz
* Jacob Madsen (now at Universität Wien)

----------
References
----------

.. [JAMAD1] Jacob Madsen, Pei Liu, Jens Kling, Jakob Birkedal Wagner,
	    Thomas Willum Hansen, Ole Winther and Jakob Schiøtz: *A
	    Deep Learning Approach to Identify Local Structures in
	    Atomic‐Resolution Transmission Electron Microscopy
	    Images*, Adv. Theory Simul. (in press, 2018).  DOI:
	    `10.1002/adts.201800037
	    <https://doi.org/10.1002/adts.201800037>`_.  Preprint
	    available at https://arxiv.org/abs/1802.03008

	    
