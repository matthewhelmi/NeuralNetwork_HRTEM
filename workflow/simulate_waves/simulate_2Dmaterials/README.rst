=====================
Simulation of 2D materials
=====================

This folder contains code for simulating 2D materials.  The main code is in flake.py and in maker.py.

flake.Flake produces flakes of 2D material.  It takes a prototype (a unit cell of the material), creates a sheet and cuts a flake from the sheet.  It is possible to remove random atoms (vacancies) and entire columns (removing e.g. the two sulphur atoms on the opposite sides of MoS2).  Positions can be perturbed, and it is possible to randomly rotate and tilt the sample.

maker.py contains code for running the simulation.  It defines the class ``SampleMaker`` which has a ``.run()`` method for creating and saving the exit wave function, the sites and the classes.   The SampleMaker class is incomplete, it delegates creation of the atoms object and the site and classes data to a method ``.make_data()`` which has to be defined in a subclass.

Two sub classes handling common cases are also defined.  ``SampleMakerTrue2D`` is for 2D materials where there is always only one atom in a column.  It's ``.make_data()`` method creates the atoms by calling the (missing) ``.make_atoms()`` method (which must be defined in subclasses for specific materials), and then extracts sites and classes from the atoms.  ``SampleMakerDistinctColumns`` handles columns with more than one atom, and defines different classes for each combination of elements in the column.

Finally, maker.py defines a ``main()`` function, which takes a driver function as argument (defined in the actual scripts).  ``main()`` interprets the command line, and runs the simulation (in parallel, if specified on the command line).


The scripts produce separate materials.  They all create subclasses of ``SampleMakerTrue2D``, ``SampleMakerDistinctColumns`` or ``SampleMaker`` itself, and defines the missing function(s).  The ``make_atoms()`` method creates the sample using the ``Flake`` object.

The scripts also define a driver function which instantiates the class and call it to run the simulation.  Finally, they call the ``main()`` method with the driver function to run the simulation.

