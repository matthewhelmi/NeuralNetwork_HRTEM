#!/bin/bash
#SBATCH --job-name=MoS2-benchmark
#SBATCH --mail-type=START,END
#SBATCH --partition=xeon40
#SBATCH --output=slurm-%x-%j.out
#SBATCH --time=24:00:00
#SBATCH -N 1
#SBATCH -n 40

# Necessary modules
source ../../../../venv/bin/activate

python make_supported_MoS2.py MoS2_supported_DW 1000 -n -1 --train --debyewaller=0.003
python make_supported_MoS2.py MoS2_supported_DW 1000 -n -1 --test --debyewaller=0.003

