import numpy as np
import scipy.spatial
import matplotlib.path as mplPath
import itertools
import ase.build

class Flake:
    """Produce a flake of a 2D material.

    Usage:
    f = Flake(unitcell, (x, y))

    where unitcell is an Atoms object with the 2D material in the x-y
    plane, and (x, y) is the desired maximum dimensions in Å.

    Once the object is created, you can:
    f.make(scale=1.0)      # Create a random flake - use scale to make it smaller
    f.vacancies(n)         # Remove n random atoms
    f.rotate(angle=None)   # Rotate by a random or specified angle
    f.tilt(maxangle)       # Tilt by maxangle degrees
    f.specific_tilt(angle, direction)   # Tilt by specific amount in a direction, both in degrees.
    f.get_atoms()          # Get the atoms object
    """

    def __init__(self, prototype, size, seed=None):
        if isinstance(seed, np.random.Generator):
            self.rng = seed
        else:
            self.rng = np.random.default_rng(seed)
        # Check that the prototype is periodic along x and y.
        assert prototype.pbc[:2].all()

        # Check that the first axis is parallel to the x-axis, and
        # that the y axis is in the positive direction.  Determine if
        # the angle is below or above 90 degrees.
        xax, yax, zax = prototype.cell
        try:
            self.assert_sane_cell(xax, yax, zax)
        except AssertionError:
            print("Attempting to Niggli-reduce ", prototype.get_chemical_formula(mode='hill'))
            print("Cell before reduction:")
            print(prototype.cell[:])
            print("Periodicity:", prototype.pbc)
            assert np.linalg.norm(prototype.cell[2]) > 0.5   # Fail if no z-axis
            prototype.pbc = True
            ase.build.niggli_reduce(prototype)
            print("Cell after reduction:")
            print(prototype.cell[:])
            xax, yax, zax = prototype.cell
            self.assert_sane_cell(xax, yax, zax)
        costheta = np.dot(xax, yax) / np.sqrt(np.dot(xax, xax) * np.dot(yax, yax))

        # Find number of repetitions in y and x directions
        ny = int(np.ceil(size[1] / yax[1])) + 2
        nx = int(np.ceil((size[0] + np.abs(size[1] * costheta)) / xax[0])) + 2

        sheet = prototype.repeat((nx, ny, 1))
        sheet.positions += 1e-3  # Avoid rounding errors
        sheet.wrap()

        # Now cut a square out of this
        sheet.set_cell([size[0], size[1], prototype.cell[2,2]], scale_atoms=False)
        if costheta > 0:
            # Angle is acute, translate atoms
            sheet.positions -= (size[1] * costheta, 0, 0)
        s = sheet.get_scaled_positions(wrap=False)
        keep = ((s[:,0] > 0.0) * (s[:,0] < 1.0) * (s[:,1] > 0.0) * (s[:,1] < 1.0)).astype(bool)
        #print("Keeping {} of {} atoms".format(keep.sum(), len(sheet)))
        self.atoms = self.sheet = sheet[keep]
        self.size = size

    def assert_sane_cell(self, xax, yax, zax):
        assert xax[0] > 0
        assert np.isclose(xax[1], 0.0)
        assert np.isclose(xax[2], 0.0)
        assert yax[1] > 0
        assert np.isclose(yax[2], 0.0)
        assert np.isclose(zax[0], 0,0)
        assert np.isclose(zax[1], 0.0)

    def make(self, scale=1.0):
        """Cut a random flake from the sheet."""
        # Choose a set of random points, find a path around them (the
        # complex hull), and use it to cut the sheet.
        npoints = self.rng.integers(5, 25)
        scale *= 0.5 + 0.5 * self.rng.random()
        points = self.rng.random(size=(npoints, 2))
        points *= self.size
        points *= scale
        points = points[scipy.spatial.ConvexHull(points).vertices]

        # The x and y coordinates of the atoms
        xy = self.sheet.get_positions()[:,:2]

        # Center both point sets around the origin
        points -= np.mean(points, axis=0)
        xy -= np.mean(xy, axis=0)

        # Choose the atoms to cut, then cut them out.
        contained = mplPath.Path(points).contains_points(xy)
        self.atoms = self.sheet[contained]

    def vacancies(self, n , element):
        """Make n vacancies.

        The argument n can be the number of atoms to remove (an integer)
        or a float less than 1.0 indicating the fraction to remove.
        """
        atoms_bool = self.atoms.symbols == element
        element_indices = [i for i, x in enumerate(atoms_bool) if x]
        antielement_indices = [i for i, x in enumerate(atoms_bool) if not x]
        if n < 1:
            # A fraction of atoms instead of a number
            n = self.rng.uniform(0,n) # randomise the percentage
            n = int(len(element_indices) * n)
            print(f"Info: Creating {n}/{len(element_indices)} ({100*n/len(element_indices):.1f} %) {element} vacancies.")
            if n == 0:
                print(f"Warning: Trying to remove {n}/{len(self.atoms)} atoms - do nothing.")
                return n
        self.rng.shuffle(element_indices)
        keep = element_indices[n:] + antielement_indices
        self.atoms = self.atoms[keep]
        
        return n

    def holes(self, n, size=1.0):
        """Make n holes.  Like vacancies(), but remove entire columns of atoms.

        The argument n can be the number of atoms to remove (an integer)
        or a float less than 1.0 indicating the fraction to remove.
        """
        if n < 1:
            # A fraction of atoms instead of a number
            n = self.rng.uniform(0,n) # randomise the percentage
            n = int(len(self.atoms) * n)
            print(f"Info: Creating {n}/{len(self.atoms)} ({100*n/len(self.atoms):.1f} %) holes.")
            if n == 0:
                print(f"Warning: Trying to remove {n}/{len(self.atoms)} atoms - do nothing.")
                return n
        indices = np.arange(len(self.atoms))
        self.rng.shuffle(indices)
        selected = indices[:n]
        # Now remove all columns
        xy = self.atoms.get_positions()[:,:]
        kdtree = scipy.spatial.cKDTree(xy)
        remove = kdtree.query_ball_point(xy[selected], size)
        # remove is a list of list.  Concatenate them
        remove = list(itertools.chain.from_iterable(remove))
        for i in selected:
            assert i in remove
        keep = np.ones(len(self.atoms), bool)
        keep[remove] = False
        self.atoms = self.atoms[keep]

        return n

    def rotate(self, angle=None):
        """Rotate the sheet by a random or specified amount."""
        if angle is None:
            angle = 360 * self.rng.random()
        self.atoms.rotate(angle, 'z', center='COU')
        
    def tilt(self, maxangle):
        angle = maxangle * self.rng.random()
        direction = 360 * self.rng.random()
        #print("Tilting by {} degrees in direction {}".format(angle, direction))
        self.specific_tilt(angle, direction)
        
        return angle

    def specific_tilt(self, angle, direction):
        self.atoms.rotate(v='z', a=direction, center='COP')
        self.atoms.rotate(v='y', a=angle, center='COP')
        self.atoms.rotate(v='z', a=-direction, center='COP')

    def perturb_positions(self, amplitude):
        perturbation = self.rng.normal(scale=amplitude, size=self.atoms.positions.shape)
        self.atoms.positions += perturbation

    def shift(self, boundary=2.0):
        xsize = self.atoms.cell[0,0]
        ysize = self.atoms.cell[1,1]
        r = self.atoms.get_positions()
        xmin, ymin, _ = r.min(axis=0)
        xmax, ymax, _ = r.max(axis=0)
        if xmax - xmin < xsize - 2*boundary + 1e-3:
            dx = self.rng.uniform(boundary - xmin, xsize - boundary - xmax)
        else:
            dx = 0.0
        if ymax - ymin < ysize - 2*boundary + 1e-3:
            dy = self.rng.uniform(boundary - ymin, ysize - boundary - ymax)
        else:
            dy = 0.0
        self.atoms.set_positions(r + np.array([dx, dy, 0.0]))

    def stack(self, other, distance, shift=True, placement='random'):
        """Stack two 2D materials.

        If the computational cells of the two materials are different,
        the host should have the largest one.

        Optional arguments:
        shift (default: True): Shift both systems randomly inside their
        respective unit cells.
        
        placement: "above", "below" or "random" (the default) determines
        how the other system is placed with respect to this one.
        """
        if placement == 'random':
            placement = self.rng.choice(['above', 'below'])
        if shift:
            self.shift()
            other.shift()
        if hasattr(distance, '__len__'):
            assert len(distance) == 2
            distance = self.rng.uniform(*distance)
        if placement == 'above':
            dz = self.atoms.get_positions()[:,2].max() - other.atoms.get_positions()[:,2].min() + distance
        elif placement == 'below':
            dz = self.atoms.get_positions()[:,2].min() - other.atoms.get_positions()[:,2].max() - distance
        else:
            raise ValueError("Unknown placement '{}'.".format(placement))
        otheratoms = other.atoms.copy()
        otheratoms.positions += np.array([0.0, 0.0, dz])
        self.atoms.extend(otheratoms)

    def get_atoms(self, vacuum=1.5, border=2.0):
        # Remove atoms near border
        r = self.atoms.get_positions()
        size = self.size
        keep = ((r[:,0] > border) * (r[:,0] < size[0] - border)
                * (r[:,1] > border) * (r[:,1] < size[1] - border)).astype(bool)
        atoms = self.atoms[keep]
        atoms.center(vacuum=vacuum, axis=2)
        return atoms
