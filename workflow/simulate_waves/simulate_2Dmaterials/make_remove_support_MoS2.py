from maker import SampleMaker, main
from flake import Flake
from ase import Atoms
from ase.build import mx2
from scipy.cluster.hierarchy import fcluster, linkage
import numpy as np
from make_supported_MoS2 import SupportedMoS2Maker


class RemoveSupportMoS2Maker(SupportedMoS2Maker):
    def remove_support(self, atoms):
        "Remove carbon support"
        return atoms[atoms.numbers != 6]


def makeMoS2(first_number, last_number, dir_name, npoints, sampling, beamenergy, debyewaller, distort, seed):
    size = npoints * sampling
    print("Generating images {} - {}".format(first_number, last_number))
    print("Output folder:", dir_name)
    print("Number of point in wave function: {} x {}".format(npoints, npoints))
    print("Sampling: {} Å/pixel".format(sampling))
    print("System size: {:.1f} Å x {:.1f} Å".format(size, size))
    if debyewaller:
        print("Debye-Waller factor <u^2>: {:.5f} Å^2".format(debyewaller))
    if distort:
        print("Atomic distortion: {:.3f} Å".format(distort))
    print("Seed:", seed)
    maker = RemoveSupportMoS2Maker((size, size), beamenergy, debyewaller, distort, seed)
    maker.run(first_number, last_number, dir_name, npoints, sampling)


if __name__ == "__main__":
    main(makeMoS2, (__file__, 'make_supported_MoS2.py'), extrafolders=['wave_no_support'])
    

