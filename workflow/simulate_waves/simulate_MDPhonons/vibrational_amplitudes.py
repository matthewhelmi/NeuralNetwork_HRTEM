import numpy as np
from asap3 import FullNeighborList
from frozendict import frozendict

def get_edge_vibrations(trajectories, element, rcut, nb_element, max_nb=1):
    """
    This function handles edge effects. Edge atoms are identified by lowest coordination
    number, eg. an S with 1 Mo neighbour.
    All edge atoms are mapped onto a global XY-axis where a mean and standard deviation of
    the positions are extracted.
    
    Arguments:
    trajectories: List of trajectories
    element:      Int Atomic number of the element
    rcut:         Float for cut off radius for neighbouring atoms.
                  (Tip: look at the radial distribution funciton to determine this.)
    nb_element:   Int atomic number of the neighbouring atom. If None returns total
                  coordination number
    max_nb:       Int max number of neighbours to be an edge. An S at the edge will
                  have 1 Mo neigbour.
    
    Return:
    sigma_xyz     Mean over all trajectory steps of the standard deviation of atomic
                  positions of all edge atoms
    mu_xyz        Mean over all trajectory steps of the average atomic positions of all
                  edge atoms
    """
    sigma_Txyz = np.empty((len(trajectories),3))
    mu_Txyz = np.empty((len(trajectories),3))
    for t, trajectory in enumerate(trajectories):
        # Initial configuration of the MD simulatiion
        initialatoms = trajectory[0]
        natoms = len(initialatoms)
        # Get all atomic coordinates throughout the simulation
        # and constract and average system
        P_taxyz = np.empty((len(trajectory), natoms, 3))
        for i, config in enumerate(trajectory):
            P_taxyz[i] = config.get_positions()
        avgP_axyz = np.mean(P_taxyz, axis=0)
        avgatoms = initialatoms.copy()
        avgatoms.set_positions(avgP_axyz)
        
        nblist = FullNeighborList(rCut=rcut, atoms=avgatoms)
        N = avgatoms.get_atomic_numbers()

        # Locate edge atoms
        edgeatomidx = []    # Store indices of the edge atoms
        edgenbP_axyz = []   # Positions of neighbouring elements to edge atoms
        j=0
        for i, nb in enumerate(nblist):
            if N[i] != element: # if not the element of interest
                continue
            # Number of neighbours of the given neigbouring element
            Nnb = len( N[nb][ N[nb]==nb_element ])
            if Nnb == 1: # Edge sulphur atoms
                edgeatomidx.append(i)   # index of the atom
                nb_idx = nb[ np.where(N[nb] == nb_element)[0] ] # index of the neighbouring atom
                edgenbP_axyz.append(avgatoms.positions[ nb_idx ])  # coordinates of the neighbouring atom
                j += 1
            else:
                continue
        edgeatoms = avgatoms[edgeatomidx]

        # Align each atom's y-axis to point to the nearest neighbour
        edgenbP_axyz = np.array(edgenbP_axyz).reshape((len(edgeatomidx),3))
        edgeP_axyz = edgenbP_axyz - edgeatoms.get_positions()
        edgeatoms.set_positions(edgeP_axyz)

        # Angles of each edge atom to the y-axis
        theta = np.empty(len(edgeatoms))
        y = [0,1,0]
        # Map the edge atom positions onto a shared coordinate system (x,y,z) -> (xp,yp,zp)
        # This involves a rotation to the y-axis, and then a translation to origo
        translation_axyz = np.empty((len(edgeatoms), 3))
        for i, v in enumerate(edgeatoms.positions):
           tmp = angle_between(v,y)
           theta[i] = tmp
           R = rot_matz(tmp)
           translation_axyz[i] = np.dot(R,v)
        # Angles and translations are from the average structure, now we'll map all atoms in all
        # trajectory steps
        edgePp_taxyz = np.empty((len(trajectory), len(edgeatoms), 3))
        for i, config in enumerate(trajectory): # loop over trajectory steps
            edgeatoms = config[edgeatomidx]
            P_axyz = edgenbP_axyz - edgeatoms.get_positions()
            for j, P_xyz in enumerate(P_axyz): # loop over atoms in config
                R = rot_matz(theta[j])
                edgePp_taxyz[i,j] = np.dot(R,P_xyz) - translation_axyz[j]
        sigma_Txyz[t] = np.mean(np.std(edgePp_taxyz, axis=0), axis=0)
        mu_Txyz[t] = np.mean(np.mean(edgePp_taxyz, axis=0), axis=0)
    sigma_xyz = np.mean(sigma_Txyz, axis=0)
    mu_xyz = np.mean(mu_Txyz, axis=0)

    return sigma_xyz, mu_xyz
        
def perturb_edge_atoms(atoms, sigma, mu, element, rcut, nb_element, max_nb=1, seed=None):
    """
    Perturb the edge atoms using the standard deviation and mean in cartesian directions.
    The edge atoms will be mapped into the global coordinate system, before perturbing,
    and then mapped back
    
    Arguments:
    atoms:        Atoms The atomic configuration
    sigma:        Numpy array of standard deviations in cartesian directions
    mu:           Numpy array of means in cartesian direcitons
    element:      Int Atomic number of the element
    rcut:         Float for cut off radius for neighbouring atoms.
                  (Tip: look at the radial distribution funciton to determine this.)
    nb_element:   Int atomic number of the neighbouring atom.
    max_nb:       Int max number of neighbours to be an edge. An S at the edge will
                  have 1 Mo neigbour.
    
    Return:
    patoms:       Atoms The perturbed atoms configuration
    """
    nblist = FullNeighborList(rCut=rcut, atoms=atoms)
    N = atoms.get_atomic_numbers()

    # Locate edge atoms
    edgeatomidx = []    # Store indices of the edge atoms
    edgenbP_axyz = []   # Positions of neighbouring elements to edge atoms
    j=0
    for i, nb in enumerate(nblist):
        if N[i] != element: # if not the element of interest
            continue
        # Number of neighbours of the given neigbouring element
        Nnb = len( N[nb][ N[nb]==nb_element ])
        if Nnb == 1: # Edge sulphur atoms
            edgeatomidx.append(i)   # index of the atom
            nb_idx = nb[ np.where(N[nb] == nb_element)[0] ] # index of the neighbouring atom
            edgenbP_axyz.append(atoms.positions[ nb_idx ])  # coordinates of the neighbouring atom
            j += 1
        else:
            continue
    edgeatoms = atoms[edgeatomidx]
    # Align each atom's y-axis to point to the nearest neighbour
    edgeP_axyz = np.array(edgenbP_axyz).reshape((len(edgeatomidx),3)) - edgeatoms.get_positions()
    edgeatoms.set_positions(edgeP_axyz)

    # Angles of each edge atom to the y-axis
    theta = np.empty(len(edgeatoms))
    y = [0,1,0]
    # Map the edge atom positions onto a shared coordinate system (x,y,z) -> (xp,yp,zp)
    # This involves a rotation to the y-axis, and then a translation to origo
    translation_axyz = np.empty((len(edgeatoms), 3))
    for i, v in enumerate(edgeatoms.positions):
       tmp = angle_between(v,y)
       theta[i] = tmp
       R = rot_matz(tmp)
       translation_axyz[i] = np.dot(R,v)

    # Perturb atoms and transform back
    rng = np.random.default_rng(seed=seed)
    newP_axyz = np.empty((len(edgeatoms),3))
    for i in np.arange(len(edgeatoms)):
        newPp_x = rng.normal(loc=mu[0], scale=sigma[0], size=None)
        newPp_y = rng.normal(loc=mu[1], scale=sigma[1], size=None)
        newPp_z = rng.normal(loc=mu[2], scale=sigma[2], size=None)
        
        newPp_xyz = [newPp_x, newPp_y, newPp_z]
        newPp_xyz_tmp = newPp_xyz + translation_axyz[i]
        R = rot_matz(theta[i])
        newP_axyz[i] = np.dot(R.T,newPp_xyz_tmp)

    # Construct new atomic system
    patoms = atoms.copy()
    translate = np.zeros((len(patoms),3))
    translate[edgeatomidx] = newP_axyz - edgeatoms.get_positions()
    patoms.translate(translate)

    return patoms

def rot_matz(theta):
    """
    A rotation matrix about the z axis
    """
    r = np.array(
        ((np.cos(theta), -np.sin(theta), 0),
         (np.sin(theta),  np.cos(theta), 0),
         (0, 0, 1))
                )
    return r
    
def angle_between(p1, p2):
    """
    Angle between two points
    """
    ang1 = np.arctan2(p1[0],p1[1])
    ang2 = np.arctan2(p2[0],p1[1])
    return (ang1 - ang2) % (2 * np.pi)


def get_vibrational_amplitudes(trajectories, element, rcut, nb_element=None):
    """
    Will return a frozendict of the mean of the standard deviation of positions in
    each Cartesian direction for each unique coordination number of an element to
    a neighbouring element.
    
    Arguments:
    trajectories: List of trajectories
    element:      Int Atomic number of the element
    nb_element:   Int atomic number of the neighbouring atom. If None returns total
                  coordination number
    rcut:         Float for cut off radius for neighbouring atoms.
                  (Tip: look at the radial distribution funciton to determine this.)
    
    Return:
    va: Frozendict of the vibrational amplitudes sorted by coordination number
    """
    va = {}     # Dictionary for vibrational amplitudes
    er = {}     # Dictionary for standard error of the vibrational amplitudes
    A_cxyz = [] # This list will contain all amplitudes for coordination number (c) and cartesian
                # coordinates (xyz) of the atom
    for trajectory in trajectories:
        # Final atomic configuration
        finalatoms = trajectory[-1]
        natoms = len(finalatoms)
    
        # Obtain all cartesian positions of each atom throughout the MD steps
        allpos = np.empty(( len(trajectory), natoms, 3))
        for i, atoms in enumerate(trajectory):
            pos = atoms.get_positions()
            allpos[i] = pos
    
        # Get full neighbours list with specified cut off radius in Ångstrøm using atoms
        # with average positions form the MD simulation
        avgpos = np.mean(allpos,axis=0)
        avgatoms = finalatoms.copy()
        avgatoms.set_positions(avgpos)
        nblist = FullNeighborList(rCut=rcut, atoms=avgatoms)
    
        # Get list of atomic numbers in atoms
        N = finalatoms.get_atomic_numbers()
    
        # Standard deviation of the positions of each atom in each (xyz) cartesian direction
        # These are all the amplitudes
        A_xyz = np.std(allpos, axis=0)
        # Extract coordination number for specified element
        for i, nb in enumerate(nblist):
            if N[i] != element: # If this index is not for the element of interest
                continue
            if nb_element is not None:
                # Number of neighbouring atoms of the specified element
                Nnb = len( N[nb][ N[nb] == nb_element ] )
            else:
                # Total number of neighbouring atoms (any element)
                Nnb = len(N[nb])
            # Combine the amplitudes with the coordination number
            A_cxyz.append(np.insert(A_xyz[i],0,Nnb))
            
    # Obtain unique coordination numbers to mean over
    A_cxyz = np.asarray(A_cxyz)
    uniquecn = np.unique(A_cxyz[:,0])
    for cn in uniquecn:
        tmp_cnxyz = A_cxyz[np.where(A_cxyz[:,0] == cn)] # Grab all xyz with the specific coordination number
        va[cn] = np.mean(tmp_cnxyz,axis=0)[1:]          # Mean of amplitudes over coordination number
        
        stderr = np.std(tmp_cnxyz,axis=0,ddof=0)[1:] / np.sqrt(tmp_cnxyz.shape[0]) # Standard error
        er[cn] = np.nan_to_num(stderr) # with only one datapoint the std is not defined!
    
    va = frozendict(va)
    er = frozendict(er)
    return va, er

def perturb_atoms(atoms, rcut, ele_A, va_A, ele_B=None, va_B=None, seed=None):
    """
    Will return atoms perturbed by a Gaussian distribution based on the given vibrational amplitudes
    
    Arguments:
    atoms:  Atoms of the system of interest
    rcut:   Float for cut off radius for neighbouring atoms
    ele_A:  Atomic number of atomic species A in atoms
    va_A:   FrozenDict/Dict containing the Cartesian vibrational amplitudes for each coordination number of 
            atomic species A in atom
    ele_B:  Atomic number of atomic species B in atoms. None if there is only one species.
    va_B:   FrozenDict/Dict containing the Cartesian vibrational amplitudes for each coordination number of 
            atomic species B in atoms. None if there is only one species.
    seed:   Int/Generator Seed to reproduce random peturbations.
            
    
    Return:
    patoms: perturbed atoms
    """
    
    # Random number generator for reproducibility
    if isinstance(seed, np.random.Generator):
        rng = seed
    rng = np.random.default_rng(seed=seed)
 
    # Get the full neighbour list
    nblist = FullNeighborList(rCut=rcut, atoms=atoms)
    # Get list of atomic numbers in atoms
    N = atoms.get_atomic_numbers()
    if len(np.unique(N)) > 1 and ele_B is None:
        print("Only one element specified in multi-component system.")    

    # Extract element A from atoms
    cnA_i = [] # atom index and coordination number
    for i, nb in enumerate(nblist):
        if N[i] != ele_A: # If this index is not for the element of interest
            continue
        else:
            # Total number of neighbouring atoms
            Nnb = len(N[nb])
        cnA_i.append((i, Nnb))
    cnA_i = np.asarray(cnA_i)
    
    #perturb positions
    pos_axyz = atoms.get_positions()
    for i, c in cnA_i:
        va_xyz = va_A[c] # Extract amplitudes from dictionary
        pos_axyz[i,0] += rng.normal(loc=0.0, scale=va_xyz[0], size=None) # Perturb x
        pos_axyz[i,1] += rng.normal(loc=0.0, scale=va_xyz[1], size=None) # Perturb y
        pos_axyz[i,2] += rng.normal(loc=0.0, scale=va_xyz[2], size=None) # Perturb z
    
    # For the second species in the atomic system if present
    if ele_B is not None:    
        # Extract element B from atoms
        cnB_i = [] # atom index and coordination number
        for i, nb in enumerate(nblist):
            if N[i] != ele_B: # If this index is not for the element of interest
                continue
            else:
                # Total number of neighbouring atoms
                Nnb = len(N[nb])
            cnB_i.append((i, Nnb))
        cnB_i = np.asarray(cnB_i)
        
        for i, c in cnB_i:
            va_xyz = va_B[c] # Extract amplitudes from dictionary
            pos_axyz[i,0] += rng.normal(loc=0.0, scale=va_xyz[0], size=None) # Perturb x
            pos_axyz[i,1] += rng.normal(loc=0.0, scale=va_xyz[1], size=None) # Perturb y
            pos_axyz[i,2] += rng.normal(loc=0.0, scale=va_xyz[2], size=None) # Perturb z
    
    patoms = atoms.copy()
    patoms.set_positions(pos_axyz)
    return patoms
