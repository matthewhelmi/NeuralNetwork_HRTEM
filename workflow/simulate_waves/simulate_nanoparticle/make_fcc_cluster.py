## for importing modules from parent directories
import sys
## import modules
import numpy as np
import matplotlib.pyplot as plt
import ase.io
import ase.build
import os
import time
import shutil
import argparse
import subprocess
from ase import Atoms
from ase.cluster.cubic import FaceCenteredCubic
from scipy.cluster.hierarchy import fcluster, linkage
from temnn.reproducible import make_reproducible
from abtem.potentials import Potential
from abtem.waves import PlaneWave
import multiprocessing

def driver2(args):
    driver(**args)
    
def driver(first_number, last_number, dir_name, N, sampling, element, tilt, rng):
    """Create nanoparticles, their exit waves and the ground truth for training.

    This function is creating a sequence of nanoparticles.  It is
    intended to be called in multiple processes in parallel, so the
    start and end number of the sequence is specified.

    first_number: int
        Sequence number of the first nanoparticle to be generated.
    last_number: int
        One more than the last nanoparticle to be generated.
    dir_name: string
        Name of the folder where the output is placed.
    N: int
        Size of the wavefunction array (will be N*N 128-bit complex numbers).
    sampling: float
        Resolution of the wavefunctions, in Angstrom per pixel.
    element: str
        Chemical element of the nanoparticle
    tilt: float
        Maximal off-axis tilt of nanoparticle, in degrees.  1.0 to 2.0 are good values.
    seed: np.random.SeedSequence or similar suitable seed
        Seed for the random number generator.
    """
    # Parameters
    L=sampling*N   # Cell size in Angstrom

    # Initialize random generator
    #rng = np.random.default_rng(seed)
    
    # Create the models
    # The first mention of miller indices inclues the family of planes
    # The second mention allows for truncation of that specific layer
    for i in range(first_number, last_number):
        
        # Generate a number layers for a family of surfaces
        l100 = rng.integers(low=7, high=20, endpoint=True)
        l110 = rng.integers(low=7, high=20, endpoint=True)
        l111 = rng.integers(low=7, high=20, endpoint=True)
        
        # Truncate specific layers (either 100 or 111)
        trunc_choice = rng.choice([100, 111])
        if trunc_choice == 100:
            t100 = rng.integers(low=-2, high=l100)
            surfaces = [(1, 0, 0), (1, 1, 0), (1, 1, 1), (1, 0, 0)]
            layers = [l100, l110, l111, t100]
        elif trunc_choice == 111:
            t111 = rng.integers(low=-2, high=l111)
            surfaces = [(1, 0, 0), (1, 1, 0), (1, 1, 1), (1, 1, 1)]
            layers = [l100, l110, l111, t111]
        
        # Construct atoms (Uses ASE.data for lattice constant)
        atoms = FaceCenteredCubic(element, surfaces, layers)

        # Rotate to a 110 zone axis
        atoms.rotate(v='y',a=45.0, center='COU')

        limit = 2.0                    # Minimal amount of space along edges.
        atoms.center(vacuum=0)
        #size=np.diag(atoms.get_cell())
        
        # Apply a random tilt        
        omega=rng.random()*360
        alpha=rng.random()*tilt
        atoms.rotate(v='z', a=omega, center='COU')
        atoms.rotate(v='y', a=alpha, center='COU')
        atoms.rotate(v='z', a=-omega, center='COU')
        
        # Random in-plane rotation
        atoms.rotate(v='z', a=rng.random()*360, center='COU')
      
        # Move random amount in [limit, L - limit]
        tx = (L - 2*limit)/2 * rng.random()
        ty = (L - 2*limit)/2 * rng.random()
        atoms.translate((tx, ty, 0))
  
        atoms_pos = atoms.positions
        keep = np.ones(len(atoms),bool)
        for j, p in enumerate(atoms_pos):
            if (p[0]>L-limit or p[1]>L-limit or p[2]>L-limit):
                keep[j] *= False
            elif (p[0]<0+limit or p[1]<0+limit or p[2]<0+limit):
                keep[j] *= False
        atoms = atoms[keep]
        atoms = Atoms(['Au']*len(atoms), atoms.positions) # abTEM wants a clean Atoms object
        atoms.set_cell((L,)*3)
        print('Removed {} atoms. {} Remaining.'.format(len(keep[keep==False]),len(atoms)))

        # Find the atomic columns
        positions = atoms.get_positions()[:,:2]
        clusters = fcluster(linkage(positions), 1, criterion='distance')
        unique,indices=np.unique(clusters, return_index=True)
        # Find the positions of the columns and the number of atoms per column.
        sites = np.array([np.mean(positions[clusters==u],axis=0) for u in unique])
        heights = np.array([np.sum(clusters==u) for u in unique])

        ## *** abTEM for TEM image ***
        # Determine number of points for each potential slice
        wave_xy = np.asarray(atoms.cell[0,0], atoms.cell[1,1])
        wave_pts = int(wave_xy/sampling)
        
        # Build potential for atoms
        # Number of slices is set by atoms.cell[2,2] with a default thickenss of 0.5A
        potential = Potential(atoms,
                      sampling=sampling,
                      gpts=wave_pts, 
                      parametrization='kirkland', 
                      #slice_thickness=0.5,
                      projection='infinite')
        # Build wave
        wave = PlaneWave(
                energy=300e3     #acceleration voltage in eV
        )
        # Compute exit wave
        exit_wave = wave.multislice(potential,
                                 pbar=False)
        # Save coordinates of positions, sites, and heights
        np.savez('{0}/points/points_{1:04d}.npz'.format(dir_name,i),
                    positions=positions,
                    sites=sites, 
                    heights=heights
                )
        
        try:
            diameter = atoms.get_diameter()
        except AttributeError:
            diameter = None
        # Save wavefunction in hdf5 format along (array, energy, extent)
        exit_wave.write('{0}/wave/wave_{1:04d}'.format(dir_name,i))
        #wave.save('{0}/wave/wave_{1:04d}.npz'.format(dir_name,i))
        ase.io.write('{0}/model/model_{1:04d}.cfg'.format(dir_name,i),atoms)
        print('   Constructed Nanoparticle {:2d} w/ diameter {} and {:5d} atoms'.format(i,
                                                     diameter,
                                                     len(atoms),
                                                     flush = True))

def_resolution=1200
def_sampling=0.05

if __name__ == "__main__":
    # Use the standard argument parser - but also save the names of the positional parameters for
    # creating the reproducibility file.
    parser = argparse.ArgumentParser()
    positionalparams = ('folder', 'element', 'tilt', 'number')
    parser.add_argument("folder", help="The name of the folder (in /home/niflheim2/hrid) where the output is placed.")
    parser.add_argument("element", help="Chemical symbol of the desired material.", type=str)
    parser.add_argument("tilt", help="Maximal off-axis tilt of the nanoparticle", type=float)
    parser.add_argument("number", type=int,
                        help="The desired number of training/testing examples (incl. any that are already done).")
    parser.add_argument("--sampling", type=float, default=def_sampling,
                        help="Sampling of wave function in Angstrom per pixel")
    parser.add_argument("--resolution", type=int, default=def_resolution,
                            help="Resolution of wave function in pixels.")
    parser.add_argument("-s", "--start", type=int, default=0,
                        help="Starting value (if some training data was already made).")
    parser.add_argument("-n", "--numproc", type=int, default=1,
                        help="Number of processes to use (CPU cores).  Use -1 for all available cores.")
    parser.add_argument('--train', dest='test', action='store_false',
                       help="Training data? (default)")
    parser.add_argument('--test', dest='test', action='store_true',
                       help="Test data?")
    parser.add_argument('--seed', type=int, default=None,
                        help="Seed for the random number generator")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    if args.numproc > args.number - args.start:
        raise ValueError("Cannot run on more CPU cores than the number of training data.")

    if args.numproc < -1 or args.numproc == 0:
        raise ValueError("Number of CPU cores must be a positive integer, or -1 for all cores.")

    numproc = args.numproc
    if numproc == -1:
        if 'LSB_MAX_NUM_PROCESSORS' in os.environ:
            numproc = int(os.environ['LSB_MAX_NUM_PROCESSORS'])
        else:
            numproc = os.cpu_count()

    dir_name = os.path.join('/home/niflheim2/mhlla/hrid', 'simulation_data', args.folder)
    seed = np.random.SeedSequence(args.seed)
        
    # Make sure working folders exist
    # Generate test set?
    if args.test==True:
        dir_name += '-test'
    if not os.path.exists(dir_name):
        print("Creating folder", dir_name)
        for subf in ['wave', 'model', 'points']:
            os.makedirs(os.path.join(dir_name, subf))
            
    print("Generating samples {} to {} in folder {} using {} process(es)\n".format(
        args.start, args.number, dir_name, numproc))
    print("np.random.SeedSequence entropy:", seed.entropy)
    print("\nElement: {}".format(args.element))

    # Keep a copy of this script for reference ...
    shutil.copy2(__file__, dir_name)
    # ... and write a shell script to reproduce this run.
    argdict = vars(args).copy()
    argdict['seed'] = seed.entropy
    argdict['numproc'] = numproc
    make_reproducible(os.path.join(dir_name, 'reproduce.sh'),
                        parser.prog,
                        argdict,
                        positionalparams)
    before = time.time()

    if numproc == 1:
        # Running on a single core.
        rng = np.random.default_rng(seed)
        driver(first_number=args.start,
                last_number=args.number,
                dir_name=dir_name,
                N=args.resolution,
                sampling=args.sampling,
                element=args.element,
                tilt=args.tilt,
                rng=rng)
                #seed=seed)
    else:
        # For abTEM compatability
        multiprocessing.set_start_method('spawn')
        data = []
        ndata = args.number - args.start
        seeds = seed.spawn(numproc)
        rngs = []
        for seed in seeds:
            rngs.append(np.random.default_rng(seed))
        for i in range(numproc):
            data.append(dict(
                first_number=args.start + i * ndata // numproc,
                last_number=args.start + (i+1) * ndata // numproc,
                dir_name=dir_name,
                N=args.resolution,
                sampling=args.sampling,
                element=args.element,
                tilt=args.tilt,
                rng=rngs[i]
                #seed=seeds[i]
                ))
        with multiprocessing.Pool(numproc) as pool:
            pool.map(driver2, data)

    print("Time to simulate models: {:.2f} s.".format(time.time() - before))
