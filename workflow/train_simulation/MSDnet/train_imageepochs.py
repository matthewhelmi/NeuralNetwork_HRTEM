import sys
import os
import glob
import json
import platform
import contextlib
import time
import argparse
import shutil

import numpy as np
import cupy as cp
#os.environ['TF_CPP_MIN_LOG_LEVEL'] = '1'  # or any {'0', '1', '2'}
#os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'
import tensorflow as tf
#import tensorflow_addons as tfa
from tensorflow.keras import mixed_precision
from tensorflow.keras import layers
from tensorflow.keras.optimizers import Adam, RMSprop

from temnn.knet.ReflectionPadding2D import ReflectionPadding2D
from temnn.reproducible import make_reproducible

from skimage.filters import gaussian
from scipy.ndimage.filters import gaussian_filter
from cupyx.scipy import ndimage as cp_ndi

from datetime import datetime
from tqdm import tqdm

#########################################################
#### tensorflow mixed precision for memory efficiency ###
#########################################################
policy = mixed_precision.Policy('float32')
#policy = mixed_precision.Policy('mixed_float16')
mixed_precision.set_global_policy(policy)

#########################################################
########## construct net architecture ###################
#########################################################
def get_model(img_dim,
              num_layers,
              num_channels,
              output_layer,
              filters=3,
              kernel_size=9,
              seed=None):
    # prepare input
    inputs = tf.keras.Input(shape=img_dim)
    # prepare random dilations
    if seed is None:
        seed = 1234
    rng = np.random.default_rng(seed)
    
    # start networks
    x = inputs
    # proceeding layers
    for i in range(num_layers):
        dil = rng.integers(low=1, high=11, size=1)
        x = ReflectionPadding2D(padding=(dil[0]*(kernel_size-1),
                                dil[0]*(kernel_size-1)))(x)
        x = layers.Conv2D(
                filters=filters,
                kernel_size=kernel_size,
                padding="valid",  # valid when using reflection padding
                dilation_rate=(dil[0],dil[0]),
                kernel_initializer='RandomNormal',
                bias_initializer=tf.keras.initializers.Constant(0.1)
                )(x)
        x = layers.PReLU(
                shared_axes=[1,2],
                alpha_initializer=tf.keras.initializers.Constant(0.01)
                )(x)
        x = layers.BatchNormalization()(x)
        if i > 0:
            x = layers.concatenate([x, previous_block_activation])
        previous_block_activation = x 
    
    if output_layer == 'segment':
        if num_channels > 1:
            final_act = "softmax"
        else:
            final_act = "sigmoid"
    elif output_layer == 'regress':
        final_act = None
    # add a per-pixel score layer
    outputs = layers.Conv2D(
                filters=num_channels,
                kernel_size=1,
                activation=final_act,
                padding="same",
                kernel_initializer='RandomNormal',
                bias_initializer=tf.keras.initializers.Constant(0.1),
                dtype='float32')(x)
    
    # define the model
    model = tf.keras.Model(inputs, outputs)
    return model

#########################################################
################## data generator #######################
#########################################################
class datagenerator(tf.keras.utils.Sequence):
    def __init__(self, 
            batch_size, 
            img_size,
            data_paths,
            params_paths,
            input_channels,
            output_channels,
            normalisation_distance,
            augment=True):
         
         self.batch_size = batch_size
         self.img_size = img_size
         self.data_paths = data_paths
         self.params_paths = params_paths
         self.input_channels = input_channels
         self.output_channels = output_channels
         self.normalisation_distance = normalisation_distance
         self.augment = augment
         
         assert len(self.data_paths) == len(self.params_paths)
         self.n = len(self.data_paths)
         self.indexes = np.arange(self.n)

    def on_epoch_end(self):
        'updates indexes after each epoch'
        np.random.shuffle(self.indexes)
    
    def __getitem__(self, index):
        i = self.indexes[index * self.batch_size]
        batch_data_paths = self.data_paths[i : i + self.batch_size]
        batch_params_paths = self.params_paths[i : i + self.batch_size]

        return self.__dataloader(self.img_size,
                batch_data_paths, batch_params_paths,
                self.input_channels, self.output_channels,
                self.normalisation_distance)
    
    def __len__(self):
        return self.n // self.batch_size

    #################### data loader ########################
    def __dataloader(self, 
            img_size,
            data_paths,
            param_paths,
            input_channels,
            output_channels,
            normalisation_distance):
        x = np.zeros((len(data_paths), img_size[0], img_size[1], input_channels))
        y = np.zeros((len(data_paths), img_size[0], img_size[1], output_channels))
        for j, (data_path, param_path) in enumerate(zip(data_paths, param_paths)):
            with open(param_path) as json_file:
                p = json.load(json_file)
            sigma = normalisation_distance/p['sampling']
            data = np.load(data_path)
            
            img_tmp = data['image'][:,:img_size[0],:img_size[1],:]
            lbl_tmp = data['label'][:,:img_size[0],:img_size[1],:]
            img_tmp = cp_local_normalise(img_tmp[0,:,:,:], sigma, sigma)
            img_tmp.shape = (1,) + img_tmp.shape
            if self.augment:
                img_tmp = random_brightness(img_tmp, -0.1, 0.1)
                img_tmp = random_contrast(img_tmp, 0.9, 1.1)
                img_tmp = random_gamma(img_tmp, 0.9, 1.1)
                img_tmp, lbl_tmp = random_flip(img_tmp, lbl_tmp)
            x[j] = img_tmp
            y[j] = lbl_tmp

        return x, y

#########################################################
################## image processing #####################
#########################################################
def cp_local_normalise(image, sigma1, sigma2):
    image = cp.asarray(image)
    channels = image.shape[-1]
    if channels > 1:
        B=cp.zeros_like(image[:,:,0])
        S=cp.zeros_like(image[:,:,0])
        for i in range(channels):
            B += cp_ndi.gaussian_filter(image[:,:,i],
                                        (sigma1,sigma1),
                                        mode='reflect')
        for i in range(channels):
            image[:,:,i] = image[:,:,i] - B/channels
        
        for i in range(channels):
            S += cp.sqrt(cp_ndi.gaussian_filter(image[:,:,i]**2,
                                                (sigma2,sigma2),
                                                mode='reflect'))
        for i in range(channels):
            image[:,:,i] = image[:,:,i] / (S/channels)
    else:
        image[:,:,0] = image[:,:,0] - cp_ndi.gaussian_filter(image[:,:,0],
                                                             (sigma1,sigma1),
                                                             mode='reflect')
        image[:,:,0] = image[:,:,0] / cp.sqrt(cp_ndi.gaussian_filter(image[:,:,0]**2,
                                                                     (sigma2,sigma2),
                                                                      mode='reflect'))   
    return image.get()

def random_flip(images, labels, rnd=np.random.rand):
    for i in range(len(images)):
        if rnd() < .5:
            images[i,:,:,:] = np.fliplr(images[i,:,:,:])
            labels[i,:,:,:] = np.fliplr(labels[i,:,:,:])
            
        if rnd() < .5:
            images[i,:,:,:] = np.flipud(images[i,:,:,:])
            labels[i,:,:,:] = np.flipud(labels[i,:,:,:])
    return images,labels

def random_brightness(images, low, high, rnd=np.random.uniform):
    for i in range(len(images)):
        images[i,:,:,0]=images[i,:,:,0]+rnd(low,high)
    return images

def random_contrast(images, low, high, rnd=np.random.uniform):
    for i in range(len(images)):
        mean=np.mean(images[i,:,:,0])
        images[i,:,:,0]=(images[i,:,:,0]-mean)*rnd(low,high)+mean
    return images
    
def random_gamma(images, low, high, rnd=np.random.uniform):
    for i in range(len(images)):
        min=np.min(images[i,:,:,0])
        images[i,:,:,0]=(images[i,:,:,0]-min)*rnd(low,high)+min
    return images

#########################################################
################## START OF MAIN CODE ###################
#########################################################
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    positionalparams = ('epochs', 'layers', 'datf', 'vdatf', 'outf')
    parser.add_argument("epochs", default=10, type=int,
            help="Number of training epochs.")
    parser.add_argument("layers", default=10, type=int,
            help="Number of MSDnet layers.")
    parser.add_argument("datf",
            help="The path and name of the folder where the training data is placed.")
    parser.add_argument("vdatf",
            help="The path and name of the folder where the validation data is placed..")
    parser.add_argument("outf",
            help="The name of the folder where the output is placed.")
    parser.add_argument("--filters", default=3, type=int,
                        help="Number of filters in MSDnet (default: 3)")
    parser.add_argument("--kernel", default=9, type=int,
                        help="Kernel size in MSDnet (default: 9)")
    parser.add_argument("--restart", default=None, type=int, help="Restart at epoch N.")
    parser.add_argument("--epochsave", default=1, type=int,
                        help="Interval for saving checkpoints (default: every 5 epochs)")
    parser.add_argument("--adam", default=None, type=str,
                        help="Parameters for Adam optimizers, as a string containing a dict.")
    parser.add_argument("--rmsprop", default=None, type=float,
                        help="Use RMSprop with this learning rate (instead of Adam).")
    parser.add_argument("--limitdata", default=None, type=int,
                        help="Limit training to this number of images per epoch (default: all available)")
    args = parser.parse_args()

    ### Create output folder ###
    if not args.outf.endswith('/'):
        outf = args.outf + '/'
    else:
        outf = args.outf
    if not os.path.exists(outf):
        print('Creating ', outf)
        os.makedirs(outf)
    ### Keep a copy of this script for reference ###
    shutil.copy2(__file__, outf)
    argdict = vars(args).copy()
    make_reproducible(os.path.join(outf, 'reproduce_train.sh'),
                      parser.prog,
                      argdict,
                      positionalparams)
    
    #########################################################
    ################ determine number of gpus ###############
    ### if there are gpus availables they will be located and
    ### utilised
    #########################################################
    cudavar = 'cuda_visible_devices'
    if cudavar in os.environ:
        cudadev = os.environ[cudavar]
        n_gpu = len(cudadev.split(','))
        print(cudavar, '=', cudadev)
        print("found {} gpu devices".format(n_gpu))
    else:
        n_gpu = 1
    # print on which host this is running (useful for troubleshooting on clusters).
    print("{}: running on host '{}'".format(
        datetime.now().strftime("%Y%m%d-%H%M%S"),
        platform.node()
    ))
    ## set up multi gpu
    btch = n_gpu*2 # set batch size to the number of gpu's*2
    if n_gpu > 1: # if multi-gpu parallelisation is supported
        strategy = tf.distribute.mirroredstrategy()
        strategy_scope = strategy.scope
        print("*** replicas:", strategy.num_replicas_in_sync)
        print(strategy)
        assert strategy.num_replicas_in_sync == n_gpu
    else: # otherwise run on a single gpu in serial
        strategy_scope = contextlib.nullcontext
    
    ## load metadata
    datf = args.datf 
    vdatf = args.vdatf
    with open(os.path.join(datf, 'parameters.json')) as json_file:
        par = json.load(json_file)
    with open(os.path.join(vdatf, 'parameters.json')) as json_file:
        vpar = json.load(json_file)
    imgdim = tuple(par['image_size']) # spatial dimensions of input/output
    images_per_epoch = par['images_per_epoch']
    image_epochs = par['image_epochs']
    validation_images_per_epoch = vpar['images_per_epoch']
    assert vpar['image_epochs'] == 1, "Expected a single validation epoch"
    if par.get('multifocus', None):
        chan_in = par['multifocus'][0]
    else:
        chan_in = 1 # depth of input data
    chan_out = par['num_classes'] # number of predicted class labels
    print('Input channels: ', chan_in)
    print('Output channels: ', chan_out)

    if args.limitdata:
        print(f"WARNING: Limiting training to {args.limitdata} images per epoch (instead of {images_per_epoch})")
        if args.limitdata > images_per_epoch:
            raise ValueError("Cannot limit training to less images than available!")
        images_per_epoch = args.limitdata
    ## compile neural network
    label_type = par['label']
    if label_type == 'Mask' or label_type == 'Disk':
        output_layer = 'segment'
        if chan_out > 1:
            loss = 'categorical_crossentropy'
        else:
            loss = 'binary_crossentropy'
        metrics = ['accuracy']
        metric_labels = ['accuracy']
        for i in np.arange(chan_out):
            metrics.append(tf.keras.metrics.Precision(class_id=i, name=f'precision_{i}'))
            metric_labels.append(f'precision_{i}') 
        for i in np.arange(chan_out):
            metrics.append(tf.keras.metrics.Recall(class_id=i, name=f'recall_{i}')) 
            metric_labels.append(f'recall_{i}') 
    elif label_type == 'Exitwave' or label_type == 'Exitwave_no_support':
        loss = 'mse'
        output_layer = 'regress'
        metrics=[tf.keras.metrics.RootMeanSquaredError(name='rmse'),
                 tf.keras.metrics.MeanAbsoluteError(name='mae')]
        metric_labels=['rmse', 'mae']
    else:
        raise ValueError(f'Unknown label_type: {label_type}')

    print(f"MSDnet: layers={args.layers}  filters={args.filters}  kernel size={args.kernel}")
    print(f'Using {loss} as loss function')
    with strategy_scope():
        tf.keras.backend.clear_session()
        if args.restart is not None:
            model = tf.keras.models.load_model(
                        outf+'checkpoint-restart')
            initial_epoch = args.restart
        else:
            model = get_model((None,None,chan_in),
                              args.layers,
                              chan_out,
                              output_layer,
                              filters=args.filters,
                              kernel_size=args.kernel)
            initial_epoch = 0
    if args.rmsprop is not None and args.adam is not None:
        raise ValueError("You cannot specify both --rmsprop and --adam")
    if args.rmsprop:
        optim = RMSprop(args.rmsprop)
    elif args.adam is None:
        optim = Adam(amsgrad=True)
    else:
        adamargs = eval(args.adam)
        print("Adam parameters:", type(adamargs), adamargs)
        optim = Adam(**adamargs)
    model.compile(
           optimizer=optim,
           loss=loss,
           metrics=metrics
           )
    model.summary(positions=[.3, .6, .7, 1.])
    inputshape = (btch,imgdim[0],imgdim[1],chan_in)
    print('Model input shape: ', inputshape)
    outputshape = (btch,imgdim[0],imgdim[1],chan_out)
    # TODO Fix ReflectionPadding2D layer to be compatible with compute_output_shape
    # outputshape = model.compute_output_shape(inputshape) 
    print('Model output shape: ', outputshape)
    print('Input to output ratio (batch, x_dim, y_dim, channels): ', np.array(outputshape)/np.array(inputshape))
    sys.stdout.flush()

    #########################################################
    ################### prepare logfile #####################
    ### in the global variables at the top we defined some
    ### metrics which will be used to validate the network
    ### along the way. this is the log file where those
    ### metric values will be saved.
    #########################################################
    t = datetime.now().strftime("%Y%m%d-%H%M%S")
    logf = open(os.path.join(outf, t + '.log'),
                 "at",buffering=1)
    # Write headers to the logfile:
    line = ''
    for m in ['loss', ] + metric_labels:
        mm = 'val_' + m
        line = line + '{:<15s} {:<15s} '.format(m, mm)
    print(line.strip(), file=logf, flush=True)
    
    #########################################################
    ##################### training ##########################
    ### now finally we load the training data each training
    ### epoch and train the network
    #########################################################
    input_data_dir = datf + '/images_labels'
    data_paths = \
    [
        [
            os.path.join(input_data_dir, f'image_label_{e:03d}_{i:04d}.npz')
            for i in range(images_per_epoch)
        ]
        for e in range(image_epochs)
    ]
    n_data_paths = sum([len(l) for l in data_paths])    
    input_params_dir = datf + '/tem_params'
    params_paths = \
    [
        [
            os.path.join(input_params_dir, f'parameters_{e:03d}_{i:04d}')
            for i in range(images_per_epoch)
        ]
        for e in range(image_epochs)
    ]
    print('training data:', n_data_paths, ' samples')
    assert len(data_paths) == len(params_paths)
    
    #########################################################
    ################ load validation data ###################
    ### the input folders have been identified, including the
    ### validation dataset folder, here we load that in.
    ### keras will use this to validate the network after
    ### each training epoch
    #########################################################
    vinput_data_dir = vdatf + '/images_labels'
    vdata_paths = \
    [
        os.path.join(vinput_data_dir, f'image_label_000_{i:04d}.npz')
        for i in range(validation_images_per_epoch)
    ]
    vinput_params_dir = vdatf + '/tem_params'
    vparams_paths = \
    [
        os.path.join(vinput_params_dir, f'parameters_000_{i:04d}')
        for i in range(validation_images_per_epoch)
    ]
    print('validation data:', len(vdata_paths), ' samples')
    assert len(vdata_paths) == len(vparams_paths)
    val_gen = datagenerator(
            btch, imgdim, vdata_paths, vparams_paths, chan_in, chan_out,
            par['normalizedistance'],augment=False
    )
    
    epochs=args.epochs
    print('tensorflow v.{}'.format(tf.__version__))
    print("starting timing")
    before = time.time()
    sys.stdout.flush()
    val_loss = [1e3]
    for e in range(initial_epoch, epochs):
        img_e = e % image_epochs
        print('Epoch: ', e, 'Image Epoch: ', img_e)
        print(img_e*images_per_epoch,' to ',img_e*images_per_epoch+images_per_epoch)
        assert len(data_paths[img_e]) == len(params_paths[img_e]) == images_per_epoch
        train_gen = datagenerator(
                btch, imgdim,
                data_paths[img_e],
                params_paths[img_e],
                chan_in, chan_out, par['normalizedistance']
        )
        history = model.fit(
                         x=train_gen,
                         batch_size=btch,
                         epochs=1,
                         validation_data=val_gen,
                         shuffle=True,
                         verbose=2)
        if e % args.epochsave == 0:
            model.save(outf + 'checkpoint-restart')
        # print metrics
        history = history.history
        if all(i > history['val_loss'][0] for i in val_loss):
            model.save(outf + 'checkpoint-{:04d}'.format(e))
        val_loss.append(history['val_loss'][0])
        
        line = ''
        for m in ['loss',] + metric_labels:
            mm = 'val_' + m
            line = line + '{:<15.8f} {:<15.8f} '.format(history[m][0],
                                                        history[mm][0])
        print(line.strip(), file=logf, flush=True)
        
    totaltime = time.time() - before
    print("time: {} sec  ({} hours)".format(totaltime, totaltime/3600))

    keras_file = outf + 'model-0'
    model.save(keras_file)
    #tf.keras.models.save_model(model, keras_file, include_optimizer=False)
    print('saved model to:', keras_file)
    keras_file = outf + 'model-0.h5'
    model.save(keras_file)
    print('saved HDF5 model to:', keras_file)
    score = model.evaluate(
            val_gen,
            verbose=0)
    print('Test evaluation:')
    for i, v in enumerate(score):
        print('    ', model.metrics_names[i], v)
