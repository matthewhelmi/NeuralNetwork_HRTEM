#!/bin/bash

#!/bin/bash

if [[ -z "$CONDA_DEFAULT_ENV" ]]; then
    echo "Please activate the right Anaconda envronment before running this script." 1>&2
    exit
fi

if [[ -z "$CUDA_VISIBLE_DEVICES" ]]; then
    echo "Please select the desired GPU by setting CUDA_VISIBLE_DEVICES." 1>&2
    exit
fi

DATASET=Au_fcc_10_new_lowres
#DATASET=au_spharm_19_tilt1_640p

RESVLOCK=/tmp/lock-$DATASET-$$
python ../../reserve.py $RESVLOCK &
RESVPID=$!
sleep 10
echo "Locked GPU $CUDA_VISIBLE_DEVICES with lockfile $RESVLOCK and PID $RESVPID"

SIMULSUBFOLDER=simulation_data/$DATASET
SIMULFOLDER=/scratch/$USER/$SIMULSUBFOLDER
# Use this to e.g. exclude the wave subfolder when using precomputed images
RSYNCOPT='--copy-links --exclude=images --exclude=labels --exclude=debug --exclude=points --exclude=wave'


mkdir -p /scratch/$USER/$SIMULSUBFOLDER
mkdir -p /scratch/$USER/$SIMULSUBFOLDER-test
rsync -av $RSYNCOPT hrid.fysik.dtu.dk:/u/raid/mhlla/$SIMULSUBFOLDER/. $SIMULFOLDER/.
#rsync -av $RSYNCOPT themis:/scratch/$USER/$SIMULSUBFOLDER/. $SIMULFOLDER/.
rsync -av $RSYNCOPT hrid.fysik.dtu.dk:/u/raid/mhlla/$SIMULSUBFOLDER-test/. $SIMULFOLDER-test/.
#rsync -av $RSYNCOPT themis:/scratch/$USER/$SIMULSUBFOLDER-test/. $SIMULFOLDER-test/.

# Trick the script into only using 10 cpus (be nice to other users!)
export LSB_MAX_NUM_PROCESSORS=10

# Unlock
touch $RESVLOCK
wait $RESVPID

