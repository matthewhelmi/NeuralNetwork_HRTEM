import sys
sys.path.insert(0, '../../')

import os
import glob

import numpy as np

import hyperspy.api as hs

import pandas as pd

from tqdm import tqdm

from scipy import ndimage
from scipy.optimize import curve_fit

#===================================================================
# Doserate
#===================================================================
def compute_dose_rate(
    intensity,brightness,
    area,exposure):
    '''
    Function to compute dose given intensity integrated over an area
    Brightness calibration and exposure time required
    
    Return dose in units of electrons per angstrom^2 per second
    '''
    dose_rate=(intensity*brightness_cal)/(area*exposure)/100
    
    return dose_rate

#===================================================================
# FFT
#===================================================================
def fft(img):
    f = np.fft.fft2(img)
    f = np.fft.fftshift(f)

    c = f.shape[0]/2, f.shape[1]/2
    f[int(c[0]), int(c[1])] = 0.0

    return np.abs(f)

#===================================================================
# Get PSD 1D (total radial power spectrum)
#===================================================================
def GetPSD1D(psd2D):
    
    h  = psd2D.shape[0]
    w  = psd2D.shape[1]
    wc = w//2
    hc = h//2

    # create an array of integer radial distances from the center
    Y, X = np.ogrid[0:h,0:w]
    r    = np.hypot(X - wc, Y - hc).astype(np.int)

    # SUM all psd2D pixels with label 'r' for 0<=r<=wc
    # NOTE: this will miss power contributions in 'corners' r>wc
    psd1D = ndimage.mean(psd2D, r, index=np.arange(0, wc))

    psd1D_norm = psd1D/np.max(psd1D)
    
    stderr = np.zeros(wc)
    for i in np.arange(wc):
        stderr[i] = ndimage.standard_deviation(psd2D, r, index=i) / np.sqrt(len(r[r==i]))  
    
    stderr /= np.max(psd1D)
    
    return psd1D_norm[1:], r, stderr[1:]
#===================================================================

def mtf_fit(q, c1, c2, c3):
    return (1 - c1) / ( 1 + (q / c2)**c3 ) + c1

folder='/home/niflheim2/mhlla/hrid/experimental_data/wibang/20201201_ETEM_MEMS6/ROI4'
doserates=glob.glob('{}/*'.format(folder))
pbar = tqdm(doserates)
for doserate in pbar:
    pbar.set_description('Fitting {}'.format(os.path.split(doserate)[1]))
    files=glob.glob(r'{}/Hour_00/Minute_*/Second_*/*.dm4'.format(doserate))

    stackname = '/home/niflheim2/mhlla/hrid/experimental_data/wibang_out/MTFcomparison/20201201_ETEM_MEMS6/{}/hyperspy_stacks/stack_{}.hspy'.format(
            os.path.split(folder)[1],os.path.split(doserate)[1])
    if not os.path.exists(stackname):
        try:
            exp_hs=hs.load(files,lazy=True,stack=True)
            # Saving the whole stack as a hyperspy object in this new output folder (you could do this as a tif or hdf5)
            # This is smaller than the combination of all the dm4 files
            exp_hs.save(stackname,overwrite=True)
        except ValueError:
            continue
    # Now I can load the hyperspy file rather than use the lazy-loaded individual files
    exp_hs=hs.load(stackname,lazy=False)
   
    # Calcuate dose rate
    stack_metadata=exp_hs.original_metadata.stack_elements.as_dictionary()
    exps = []
    for frame_index in np.arange(len(files)):
        frame_metadata=stack_metadata['element{}'.format(frame_index)]
    
        exp_hs.axes_manager.navigation_axes[0].scale=frame_metadata['original_metadata']['ImageList']['TagGroup0']['ImageTags']['DataBar']['Exposure Time (s)']
        exp_hs.axes_manager.navigation_axes[0].units='s'
        exp_hs.axes_manager.navigation_axes[0].name='t'
        exp_hs.axes_manager
    
        brightness_cal = frame_metadata['original_metadata']['ImageList']['TagGroup0']['ImageData']['Calibrations']['Brightness']['Scale']
        exposure = frame_metadata['original_metadata']['ImageList']['TagGroup0']['ImageTags']['DataBar']['Exposure Time (s)']
    
        x_cal,y_cal = exp_hs.axes_manager.signal_axes[0].scale, exp_hs.axes_manager.signal_axes[1].scale
        x_size,y_size = exp_hs.axes_manager.signal_axes[0].size, exp_hs.axes_manager.signal_axes[1].size
    
        x_nm, y_nm = x_cal*x_size, y_cal*y_size
    
        exps.append(exp_hs.data[frame_index])
    rect = hs.roi.RectangularROI(left=0, right=2, top=0, bottom=y_nm-3.5)
    exp_hs.plot(navigator='slider')
    roi = rect.interactive(exp_hs)
    roi_data = roi.data
    dose_rate_list = []
    area = roi.inav[0].data.shape[0]*roi.inav[0].data.shape[1]*(x_cal*y_cal)
    for n in range(0,roi_data.shape[0]):
        dose_rate_list.append(compute_dose_rate(roi_data[n].sum(),brightness_cal,area,exposure))
    dose_rates=np.array(dose_rate_list)
    dr = np.mean(dose_rates)
    print(dr)

    size = 250
    # Averaging over PSD1DS
    exp_psd2ds = []
    for i, exp in enumerate(exps):
        #exp_roi = (0,exp.shape[0]-size)
        #exp_roi = (exp.shape[1]-size,0)
        exp_roi = (0,0)
        roi = exp[ exp_roi[1]:exp_roi[1]+size, exp_roi[0]:exp_roi[0]+size ]
        exp_psd2ds.append(fft(roi))

    exp_psd1d = np.zeros(int(size/2 - 1))
    _, exp_azmask, _ = GetPSD1D(exp_psd2ds[0])
    for exp_psd2d in exp_psd2ds:
        exp_psd1d += GetPSD1D(exp_psd2d)[0]
    exp_psd1d /= len(exp_psd2ds)
    exp_psd1d /= np.max(exp_psd1d)
    
    s_x = exp_hs.axes_manager.signal_axes[0].scale*10 # sampling in Å/px
    s_y = exp_hs.axes_manager.signal_axes[1].scale*10 # sampling in Å/px
    
    efreqx = np.fft.fftshift(np.fft.fftfreq(roi.shape[0], d=s_x)) * s_x
    efreqy = np.fft.fftshift(np.fft.fftfreq(roi.shape[1], d=s_y)) * s_y
    
    x = efreqx[int((roi.shape[0]/2)+1):]
    f = exp_psd1d
    popt, _ = curve_fit(mtf_fit, x, f, maxfev=10000)
    
    # residual sum of squares
    ss_res_fit = np.sum((f - mtf_fit(x,*popt)) ** 2)
    # total sum of squares
    ss_tot_fit = np.sum((f - np.mean(f)) ** 2)
    # r-squared
    r2_fit = 1 - (ss_res_fit / ss_tot_fit)
   
    outfolder = '/home/niflheim2/mhlla/hrid/experimental_data/wibang_out/mtf_parameters/{}/{}'.format(
            os.path.split(folder)[1], os.path.split(doserate)[1])
    if not os.path.exists(outfolder):
        os.makedirs(outfolder)
    outfile = outfolder + '/parameters_pdDataFrame'
    parameters = pd.DataFrame([[popt[0],popt[1],popt[2],r2_fit,dr]], columns=('c1', 'c2', 'c3', 'r2','doserate'))
    parameters.to_json(outfile)
