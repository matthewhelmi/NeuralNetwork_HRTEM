import json

p = {
    # Specify the type of task by the type of label
    # Mask, Blob, Disk, Gaussian, Exitwave
    'label': "Mask", 
    #'label': "Disk", 
    #'label': "Exitwave", 

    # Number of images (at different microscope parameters) per exit wave.
    # (Only applies to training data)
    'image_epochs': 10,
    
    # Size of the images during training (x, y)
    #'image_size': (640, 640),
    'image_size': (512, 512),
    
    # Stoachastic noise model to apply
    # poisson - shot noise approximation
    # None - no noise
    'noise': "poisson",
    
    # Number of classes in output of network, including the background
    # class.  Setting num_classes=1 means just a single class, no
    # background.  Otherwise, num_classes should be one higher than
    # the number of actual classes, to make room for the background
    # class.
    'num_classes': 1,
    #'num_classes': 4,
   
    # Should there be a background class?
    'null_class': True,

    # Spot size of atomic columns in Angstrom
    'spotsize': 0.4,

    # Image resolution range in pixels/Angstrom
    #'sampling': (0.17, 0.18),
    'sampling': (0.07, 0.08),
    #'sampling': (0.09, 0.11),

    # Focal series if not None.
    # (number of images, change in focus, random part of change).
    'multifocus': None,   
    #'multifocus': (3, 50.0, 1.0),

    ## Noise parameters 
    # Range of the logarithm (base 10) of the dose in electrons/A^2
    #'log_dose': (1, 6),
    'log_dose': (2, 6),
    #'log_dose': (4, 6),
    
    # Range of blur
    'blur': (0.1, 0.8),

    # Range of focal spread
    'focal_spread': (5, 20),

    ## CTF Parameters (values from microscope at Nanolab)
    # Defocus range in Angstrom
    'defocus': (-200, 200),
    #'defocus': (-500, -50),
    # Spherical abberation in Angstrom (1 micrometer = 1e4 A)
    'ctf_c30': (0, 1.5*83000),
    #'ctf_c30': (0, 1.5*23690),
    # 2-fold astigmatism in Angstrom
    'ctf_c12': (0, 0),
    #'ctf_c12': (0, 1.5*16),
    #'ctf_c12': (0, 1.5*34.24),
    # Axial coma in Angstrom
    'ctf_c21': (0, 0),
    #'ctf_c21': (0, 1.5*410),
    #'ctf_c21': (0, 1.5*218.5),
    # 3-fold astigmatism in Angstrom
    'ctf_c23': (0, 0),
    #'ctf_c23': (0, 1.5*1220),
    #'ctf_c23': (0, 1.5*222.5),
    # Star abberation in Angstrom
    'ctf_c32': (0, 0),
    #'ctf_c32': (0, 1.5*21000),
    #'ctf_c32': (0, 1.5*15970),
    # 4-fold astigmatism in Angstrom
    'ctf_c34': (0, 0),
    #'ctf_c34': (0, 1.5*83000),
    #'ctf_c34': (0, 1.5*18950),
    # 5-fold astigmatism in Angstrom
    'ctf_c45': (0, 0),
    #'ctf_c45': (0, 1.5*5280000),
    #'ctf_c45': (0, 1.5*460900),

    ## MTF Parameters
    # Deterministic modulation transfer function to apply
    # None - no mtf (flat spatial distribution)
    # parametric (c1,c2,c3)
    # gaussian, exponential, lorentzian(_correlated + _extra_correlated) (c1,c2)
    #"mtf": "parametric",
    "mtf": "lorentzian_extra_correlated",
    # Range of MTF parameters (same names as in paper, except c0 which is 1.0 in paper).
    #'mtf_c0': (0.12, 0.16),
    #'mtf_c1': (0, 0.1),
    'mtf_c1': (0.2, 0.6),
    #'mtf_c1': (-3.0, 0.0),
    #'mtf_c2': (0.4, 0.6),
    'mtf_c2': (0.0, 0.0),
    #'mtf_c2': (1.0, 6.0),
    #'mtf_c3': (2, 3),
    'mtf_c3': (2.0, 5.0),
    #'mtf_c3': (0.2, 1.0),

    ## Readout noise (constant lambda for additional poisson noise from the detector)
    'readout': (0.005, 0.015),

    # normalization distance in Angstrom
    'normalizedistance': 12.0,

    # How many images to save in debug folder (None=none, True=all).
    'debug': 100,
    'seed': 100
}

with open('parameters_contdos_MTF4.json', 'w') as json_file:
  json.dump(p, json_file, indent=4)
