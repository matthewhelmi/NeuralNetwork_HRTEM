import sys
sys.path.insert(0, '../../../')

import os
import glob

import hyperspy.api as hs
import cv2

import numpy as np
import matplotlib.pyplot as plt
from matplotlib_scalebar.scalebar import ScaleBar

import tensorflow as tf
import tensorflow_addons as tfa
import tensorflow.keras as keras

from skimage import filters

from codetiming import Timer
from tqdm import tqdm

from temnn.analysis.ExperimentalAnalysis import Experimental_Data, Fourier_Transform

def sliding_window(image, stepSize, windowSize):
    # slide a window across the image
    for y in range(0, image.shape[0], stepSize):
        for x in range(0, image.shape[1], stepSize):
            # yield the current window
            yield (x, y, image[y:y + windowSize[1], x:x + windowSize[0]])

folder='/home/niflheim2/mhlla/hrid/experimental_data/wibang/20210316_ETEM_MEMS8/AuCeO2_defoc9'
files=glob.glob(r'{}/Hour_00/Minute_*/Second_*/*.dm4'.format(folder))
outfolder='/home/niflheim2/mhlla/hrid/experimental_data/wibang_out/4K_segmentation/20210316_ETEM_MEMS8/AuCeO2_defoc9'
if not os.path.exists(outfolder):
    os.makedirs(outfolder)
if not os.path.exists(outfolder+'/raw'):
    os.makedirs(outfolder+'/raw')
if not os.path.exists(outfolder+'/brightness_ratio'):
    os.makedirs(outfolder+'/brightness_ratio')
if not os.path.exists(outfolder+'/summed_peaks'):
    os.makedirs(outfolder+'/summed_peaks')
print('Files contain {} frames.'.format(len(files)))

### Load trained neural network
#Trained Neural net folder
nnf = '/home/niflheim2/mhlla/hrid/MSDnet/MSDnet_10012022_imageepochs/model-0'
mod = keras.models.load_model(nnf)

winH = 512
winW = 512
step = 256
nw = 4096/step

t = Timer(name='4K Segmentation of {} frames.'.format(len(files)),
        text='{name}: Elapsed time: {:.4f} seconds')
t.start()
for fidx, f in enumerate(files):
    exp_data = Experimental_Data(f)
    image = exp_data.load()
    if exp_data.x_cal > 0.008 or exp_data.x_cal < 0.007:
        continue
    
    segraw = np.zeros(image.shape)
    segbr = np.zeros(image.shape)
    segsum = np.zeros(image.shape)

    # normalise with cupy ndimage
    tt = Timer(name="cupy normalise", text="{name}: {:.4f} seconds")
    tt.start()
    image_norm = exp_data.cp_local_standardise()
    tt.stop()

    tt = Timer(name="image scale", text="{name}: {:.4f} seconds")
    tt.start()
    scale = exp_data.get_trimmed_imagescale()           # Trim away the upper and lower 1% (default)
    tt.stop()

    tt = Timer(name="Window segmentation", text="{name}: {:.4f} seconds")
    tt.start()
    for (x, y, window) in tqdm(sliding_window(image_norm, stepSize=step, windowSize=(winW, winH)),
                               total=int(nw**2)):
        # if the window does not meet our desired window size, ignore it
        if window.shape[0] != winH or window.shape[1] != winW:
            continue
        exp_data.set_image(window)
        infer = exp_data.segment_nanoparticle(mod)
    
        segraw[y:y+winH,x:x+winW] += infer[0,:,:,0]*filters.window('hamming', window.shape)
        
        # Cut out nanoparticle
        imgcrop, crop = exp_data.crop_nanoparticle(infer)
        # Get FT
        imgcrop_ft = exp_data.fft(ftres, crop=True)
        fft_data = Fourier_Transform(imgcrop_ft)
        # Locate peaks
        peaks = fft_data.find_peaks()
        sym_peaks = fft_data.symmetric_peaks()

        br = fft_data.brightness_ratio() # brightness ratio
        segbr[y:y+winH,x:x+winW] += (infer[0,:,:,0] * br)*filters.window('hamming', window.shape)
        
        psum = fft_data.summed_peaks() # peaks sum
        segsum[y:y+winH,x:x+winW] += (infer[0,:,:,0] * psum)*filters.window('hamming', window.shape)
    tt.stop()

    exp_data.set_image(image_norm)
    ## Raw
    exp_data.set_inference(segraw)
    exp_data.plot()
    plt.savefig('{}/frame_{}.png'.format(outfolder+'/raw',str(fidx).zfill(3)))
    plt.close()
    
    ## Brightness ratio
    exp_data.set_inference(segbr)
    exp_data.plot()
    plt.savefig('{}/frame_{}.png'.format(outfolder+'/brightness_ratio',str(fidx).zfill(3)))
    plt.close()
    
    ## Summed peaks
    exp_data.set_inference(segsum)
    exp_data.plot()
    plt.savefig('{}/frame_{}.png'.format(outfolder+'/summed_peaks',str(fidx).zfill(3)))
    plt.close()
t.stop()
