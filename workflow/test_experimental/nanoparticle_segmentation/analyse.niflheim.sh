#!/bin/bash -ex
#SBATCH --job-name=Noise_performance
#SBATCH --mail-type=START,END
#SBATCH --partition=sm3090
#SBATCH --output=slurm-%x-%j.out
#SBATCH --time=7-00:00:00
#SBATCH --gres=gpu:RTX3090:1
#SBATCH -N 1                      # Minimum of 1 node
#SBATCH -n 8                      # 7 MPI processes per node

source ~/venv/bin/activate

PRE=/home/niflheim2/mhlla/hrid

python Noise_analysis_continuous_simple.py $PRE/MSDnet/MSDnet_Au_fcc_CeO2_MTF1_22112022/model-0 $PRE/experimental_data/wibang/20220301_MEMS13_continous_doserate_increment/ROI3/Au-CeO2-contdoseseries_noADCsmaller --crop 608 640
