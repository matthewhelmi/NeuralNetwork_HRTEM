Files
====

analyze_movie.py
  Analyze an image sequence with a neural network.
postprocess_expt_movie.py
  Post-process the output from the script above to generate a movie.
create_movie.dtucompute.sh
  Sends the above two scripts to a DTU Compute GPU, looping over folders of experimental data
