# Base packages
import numpy as np
import matplotlib.pyplot as plt
import skimage
import scipy
from scipy import ndimage as ndi
import matplotlib.colors

# Included in anaconda
import glob
import os

import re 
import math
from pathlib import Path 

# External packages
from moviepy.editor import *
import hyperspy.api as hs
from tqdm import tqdm
from matplotlib_scalebar.scalebar import ScaleBar

import sys
sys.path.insert(0, '../../')
import tensorflow as tf
import tensorflow.keras as keras
from temnn.knet import MSDnet, Unet
from temnn.data.mods import local_normalize

os.environ["CUDA_VISIBLE_DEVICES"]="0"

def to_hs(im_array,im_ref):
    '''
    Function to convert a numpy array (im_array) into a hyperspy object 
    and set scale equal to that of a previously defined hyperspy object (im_ref)
    
    Returns a calibrated hyperspy object im_cal
    '''
    im_cal=hs.signals.Signal2D(im_array)
    
    im_cal.axes_manager.signal_axes[0].scale=im_ref.axes_manager.signal_axes[0].scale
    im_cal.axes_manager.signal_axes[1].scale=im_ref.axes_manager.signal_axes[1].scale
  
    im_cal.axes_manager.signal_axes[0].units=im_ref.axes_manager.signal_axes[0].units
    im_cal.axes_manager.signal_axes[1].units=im_ref.axes_manager.signal_axes[1].units
    
    return im_cal

def compute_dose_rate(intensity,brightness,area,exposure):
    '''
    Function to compute dose given intensity integrated over an area
    Brightness calibration and exposure time required
    
    Return dose in units of electrons per angstrom^2 per second
    '''
    dose_rate=(intensity*brightness_cal)/(area*exposure)/100
    
    return dose_rate

def expand_mask(mask, radius=50):
    expand = mask.copy()
    for j in range(radius,mask.shape[1]):
        for k in range(radius,mask.shape[2]):
            window = mask[0,j-radius:j+radius,k-radius:k+radius,0]
            fraction = np.count_nonzero(window == 1.0)/(window.shape[0]*window.shape[1])
            if fraction > 0.1:
                expand[0,j,k,0] = 1.0
    return expand

def crop_nanoparticle(img, infer):
    # Make the inference mask binary
    binary = infer.copy()
    binary[binary > 0.01] = 1.0
    
    # Expand the inference mask
    crop = expand_mask(binary)
                
    # Crop out nanoparticle
    img_crop = img.copy()
    img_crop[crop < 1.0] = 0.0
    return img_crop, crop

def trim_imagescale(img, percent=1):
    sorted_pixels = sorted(img.flatten())
    n = len(sorted_pixels)
    trim = int(n*percent/100)
    return sorted_pixels[trim: n-trim]

def fft(img):
    f = np.fft.fft2(img)
    f = np.fft.fftshift(f)
    cf = f.shape[0]/2,f.shape[1]/2
    f = f[int(cf[0]-80):int(cf[0]+80),int(cf[1]-80):int(cf[1]+80)]
    return np.log(1+np.abs(f)) #np.abs(f), np.abs(np.log(f))

file_pattern = re.compile(r'.*?(\d+).*?')
def get_order(file):
    match = file_pattern.match(Path(file).name)
    if not match:
        return math.inf
    return int(match.groups()[0])

folder='../experimental_data/WilliamBang/20201201_ETEM_MEMS6/ROI1'
doserates=sorted(glob.glob('{}/DR*'.format(folder)),key=get_order)

output_dir='{}/processing_output'.format(folder)
# Check if an output folder exists first, if not then creates one (will only create one the first time for a new dataset)
if not os.path.exists(output_dir):
    os.mkdir(output_dir)

dose_rate_list=[]
im_list = []
scale_list = []
for DR in doserates:
    doserate = DR.split('/')[-1]
    print(doserate)
    files=glob.glob('{}/{}/Hour_00/Minute_*/Second_*/*.dm4'.format(folder,doserate))
    if not os.path.exists('{}/stack_{}.hspy'.format(output_dir,doserate)):
        try:
            i=hs.load(files,lazy=True,stack=True)
        except ValueError:
            continue
        i.save('{}/stack_{}.hspy'.format(output_dir,doserate),overwrite=True)
    i=hs.load('{}/stack_{}.hspy'.format(output_dir,doserate),lazy=False)

    stack_metadata=i.original_metadata.stack_elements.as_dictionary()
    frame_index=0
    frame_metadata=stack_metadata['element{}'.format(frame_index)]
    i.axes_manager.navigation_axes[0].scale=frame_metadata['original_metadata']['ImageList']['TagGroup0']['ImageTags']['DataBar']['Exposure Time (s)']
    i.axes_manager.navigation_axes[0].units='s'
    i.axes_manager.navigation_axes[0].name='t'
    scale_list.append((i.axes_manager.signal_axes[0].scale)*10)

    x_cal,y_cal=i.axes_manager.signal_axes[0].scale,i.axes_manager.signal_axes[1].scale
    x_size,y_size=i.axes_manager.signal_axes[0].size,i.axes_manager.signal_axes[1].size
    x_nm,y_nm=x_cal*x_size,y_cal*y_size

    im=to_hs(i.data,i)
    im_list.append(im)
    brightness_cal=frame_metadata['original_metadata']['ImageList']['TagGroup0']['ImageData']['Calibrations']['Brightness']['Scale']
    exposure=frame_metadata['original_metadata']['ImageList']['TagGroup0']['ImageTags']['DataBar']['Exposure Time (s)']
    rect=hs.roi.RectangularROI(left=0.5,right=1.5,top=y_nm-3, bottom=y_nm-2) 
    im.plot()
    roi=rect.interactive(im, color="blue")
    roi_data=roi.data

    area=roi.inav[0].data.shape[0]*roi.inav[0].data.shape[1]*(x_cal*y_cal)
    for n in tqdm(range(0,roi_data.shape[0])):
        dose_rate_list.append(compute_dose_rate(roi_data[n].sum(),brightness_cal,area,exposure))
    plt.close('all')

dose_rates=np.array(dose_rate_list)
doses=dose_rates*exposure

### Load trained neural network
n = 'MSDnet'
network = 'Au_fcc_mixed'
nnf = '../{}_precomputed_trained_data/{}'.format(n,network) #Trained Neural net folder
nnf = glob.glob(os.path.join(nnf,'model-*'))[-1] # latest model
print("Using {} CNN model in".format(n), network)
mod = keras.models.load_model(nnf)

frame_tot= len(dose_rate_list)
tot_frame_cnt = 0
for i, im in enumerate(im_list):
    for frame_index in range(0,len(im)):
        print('Stack {}/{}. Frame {} (Total Frame {}/{}).'.format(i+1,len(im_list),
                                                   frame_index+1,tot_frame_cnt+1,frame_tot))
        
        frame_dose_rate=dose_rates[tot_frame_cnt]
        frame_dose=doses[tot_frame_cnt]
        frame_cumulative_dose=np.sum(doses[0:tot_frame_cnt+1])
        frame_time=np.round(exposure*tot_frame_cnt,2)

        normalizerange = 12.0/(scale_list[i])
        img = local_normalize(im.data[frame_index], normalizerange, normalizerange)
        img.shape = (1,) + img.shape + (1,)
        infer = mod.predict(img)

        ## Processing
        infer2 = infer.copy()
        infer2[infer2 < 0.01] = np.nan
        # Cut out nanoparticle
        img_crop, crop = crop_nanoparticle(img, infer)
        # Scale for visualisation
        trimmed_pixels = trim_imagescale(img)
        scale = min(trimmed_pixels), max(trimmed_pixels)
        
        ## Plotting
        fig,ax=plt.subplots(figsize=(25.0,20.0),nrows=2,ncols=3)
        ax[0,0].imshow(img[0,:,:,0],cmap='gray',vmin=scale[0],vmax=scale[1])
        ax[0,0].text(x=0.135,y=0.88,s=r'Time : {} s'.format(frame_time),
            color='r',fontsize=20,transform=plt.gcf().transFigure)
        ax[0,0].text(x=0.135,y=0.86,s=r'Dose rate: {} e$^-$/$\AA^2$ s'.format(
            np.format_float_scientific(frame_dose_rate,2)),
            color='r',fontsize=20,transform=plt.gcf().transFigure)
        ax[0,0].text(x=0.135,y=0.56,s=r'Frame size: {} nm $\times$ {} nm'.format(
            np.round(x_nm,2),np.round(y_nm,2)),
            fontsize=14,transform=plt.gcf().transFigure)
        ax[0,0].text(x=0.135,y=0.54,s=r'Frame dose: {} e$^-$/$\AA^2$'.format(
            np.format_float_scientific(frame_dose,2)),
            color='k',fontsize=14,transform=plt.gcf().transFigure)
        ax[0,0].text(x=0.135,y=0.52,s=r'Cumulative dose: {} e$^-$/$\AA^2$'.format(
            np.format_float_scientific(frame_cumulative_dose,2)),
            color='k',fontsize=14,transform=plt.gcf().transFigure)

        ax[0,0].imshow(infer2[0,:,:,0],cmap='gnuplot_r',alpha=0.4)
        ax[0,0].add_artist(ScaleBar(x_cal,
                                    units='nm',
                                    length_fraction=0.2,
                                    color='w',
                                    frameon=False,
                                    location='lower left'))
        ax[0,0].set_xticks([])
        ax[0,0].set_yticks([])
        
        for n in np.arange(0,len(dose_rates[:tot_frame_cnt+1])):
            ax[0,1].scatter(n*exposure,dose_rates[n],color='b',marker='s',s=7)
        ax[0,1].set_xlim(0-5,frame_tot*exposure+5)
        ax[0,1].set_ylim(np.min(dose_rates),np.max(dose_rates))
        ax[0,1].set_xlabel('time (s)')
        ax[0,1].set_ylabel('dose rate (e$^-$/$\AA^2$ s)')

        ax[0,2].imshow(fft(img[0,:,:,0]), "gray")
        ax[0,2].title.set_text("Centered Spectrum")
        ax[0,2].set_xticks([])
        ax[0,2].set_yticks([])

        ax[1,0].imshow(crop[0,:,:,0],cmap='gray')
        ax[1,0].set_xticks([])
        ax[1,0].set_yticks([])
        
        ax[1,1].imshow(img_crop[0,:,:,0],cmap='gray',vmin=scale[0],vmax=scale[1])
        ax[1,1].set_xticks([])
        ax[1,1].set_yticks([])
        
        ax[1,2].imshow(fft(img_crop[0,:,:,0]), "gray")
        ax[1,2].title.set_text("Centered Spectrum")
        ax[1,2].set_xticks([])
        ax[1,2].set_yticks([])
        
        plt.subplots_adjust(bottom=0.2)
        
        plt.savefig('{}/scan_{}.png'.format(output_dir,str(tot_frame_cnt).zfill(4)))
        plt.close()
        tot_frame_cnt += 1
        
individual_frames=glob.glob('{}/scan*.png'.format(output_dir),)
clips = [ImageClip(fr).set_duration((0,0,exposure)) for fr in individual_frames]
#concat_clip = concatenate_videoclips(clips, method="compose", bg_color=(1.0,1.0,1.0))
concat_clip = concatenate_videoclips(clips)
concat_clip.write_videofile('{}/scan_video_{}.mp4'.format(output_dir,doserate),fps=1/exposure)
