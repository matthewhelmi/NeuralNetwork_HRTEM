"""Analyze a movie of electron microscopy files with neural network.

This is step one of generating a movie from a sequence of HRTEM
images.  The images must be a collection of .dm4, .dm3 or .png files
in a folder or a tree of folders (the default Hour/Minute/Second
folder structure often used for .dm4 files will work fine).

This script will run all images through a neural network, and save the
output in parallel folder structures.  If the images are in a folder
IMAGES, then the raw output will be placed in an identical folder
structure named IMAGES_prediction.  Peak analysis will be run on this
data, and this is placed in IMAGES_atoms.

Further processing can then be done e.g. with the script
postprocess_expt_movie.py.
"""
import sys
sys.path.insert(0, '../../')
import os
import shutil
import argparse
import json
import numpy as np
from glob import glob
from natsort import natsorted
from temnn.analysis.analysis import predict_multiframes_tree
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' # hide tensorflow output

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument("nnf",
        help="The folder where the trained neural network is placed")
parser.add_argument("df",
        help="The path and name of the folder where the experimental data is placed.")
parser.add_argument("outf",
        help="The path and name of the folder where the results will be placed.")
parser.add_argument("--peaks",dest='peaks',action='store_true',
        help="Analyze inference for peaks (default).")
parser.add_argument("--no-peaks",dest='peaks',action='store_false',
        help="Dont' analyze inference for peaks (for example when doing mask segmentation).")
parser.add_argument("--ft",
        help="The file extension of the experimental data (.dm4, .dm3, .png).",
        default='.dm4')
parser.add_argument("--s",
        help="Sampling in Angstrom/pixel (default: use what the CNN was trained with)",
        default=0, type=float)
parser.add_argument("--t",
        help="Threshold for detecting atoms in CNN output",
        default=0.5, type=float)
parser.add_argument("--crop", nargs='+',
        help="Crop the image (xmin, xmax, ymin, ymax)",
        default=None, type=int)
args = parser.parse_args()

# Identify neural network based on foldername
nn = args.nnf.partition("_")[0][3:]
# Import the specified neural network
if nn == 'Unet':
    from temnn.knet import Unet
    net = Unet
    print('Imported Unet architecture')
elif nn == 'MSDnet':
    from temnn.knet import MSDnet
    net = MSDnet
    print('Imported MSDnet architecture')
else:
    raise RuntimeError('Please specify a correct Neural Network instead of '+nn)

# Locate trained NN files
nng = os.path.join(args.nnf, 'model-*')
nnfls = glob(nng)
print("Found {} files matching {}".format(len(nnfls), nng))
if not nnfls:
    raise RuntimeError("No trained network found in "+nng)
nnfl = list(natsorted(nnfls))[-1]
print("Using {} CNN model in".format(nn), nnfl)

# Get the sampling from command line or from the training JSON file
s = args.s
if not s:
    parf = os.path.join(args.nnf, 'parameters.json')
    with open(parf, "rt") as pfile:
        par = json.load(pfile)
    s = np.mean(par['sampling'])
print("Experimental data in {} (file type {})".format(args.df, args.ft))

# Incase cropping is required
crop = tuple(args.crop)

### Analyse experimental images ###
predict_multiframes_tree(args.df, net=nnfl, out=args.outf, s=s, t=args.t, crop=crop, peaks=args.peaks)
