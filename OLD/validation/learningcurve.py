"""Calculate precision and recall on the testset."""

import matplotlib
matplotlib.use('AGG')

from glob import glob
import numpy as np
#import tensorflow as tf
import keras
from keras.utils import multi_gpu_model
import tensorflow as tf
from temnn.knet import net
from temnn.imagesimul.makeimages import MakeImages
from temnn.loaddata import load, load_CNN
# Peak detection
from temnn.imagesimul.evaluatepeaks import precision_recall, evaluate_result
import sys
import os
from multiprocessing import Pool
import shutil
from natsort import natsorted
import json


# This script takes one argument: the name of the folder where the
# trained neural network is placed.
if len(sys.argv) >= 2:
    graph_dir = sys.argv[1]
else:
    print("\n\nUsage: {} foldername".format(sys.argv[0]), file=sys.stderr)
    sys.exit(-1)

graph_path = os.path.join(graph_dir, '*.h5')
result = os.path.join(graph_dir, 'learningcurve.dat')
parameterfile = os.path.join(graph_dir, 'parameters.json')

with open(parameterfile, "rt") as pfile:
    parameters = json.load(pfile)

# Even if training generated debug images, we do not want to do it now.
parameters['debug'] = False

# The validation data is in a sister-folder to the training data
ddir = parameters['data_dir']
assert ddir.endswith('/')
parameters['validation_dir'] = ddir[:-1] + '-test/'

# We only need one GPU, and since we do not train we can have more images in a batch.
num_gpus = 1
batch_size = 8 * num_gpus


image_size = tuple(parameters['image_size'])
data_train = load(parameters['data_dir'])
imagestream_train = MakeImages(data_train, parameters, seed=parameters['seed'])
n_train = data_train.num_examples
print("Number of training images:", n_train)
data_valid = load(parameters['validation_dir'])
imagestream_valid = MakeImages(data_valid, parameters, seed=parameters['seed_validate'])
n_valid = data_valid.num_examples
print("Number of validation images:", n_valid)

# Keep a copy of this script for reference
shutil.copy2(__file__, graph_dir)

# The ImageStream objects have discovered how many cpus we can use
maxcpu = imagestream_train.maxcpu

# We read the number of images and the number of classes from the parameter file.
num_classes = parameters.get('num_classes', 1)
try:
    num_images = parameters['multifocus'][0]
except KeyError:
    num_images = 1

# Find all the CNN parameter files
print("Looking for CNNs in files matching", graph_path)
i = 1
cnnfiles = list(natsorted(glob(graph_path)))
print("Found {} CNN parameter files".format(len(cnnfiles)))
cnnfiles = list(enumerate(cnnfiles))
while len(cnnfiles) > 30:
    # Keep every second file, but make sure to lose the first rather than the last.
    cnnfiles = cnnfiles[::-2][::-1]

sampling = np.mean(parameters['sampling'])
with open(result, "wt") as outfile:
    if num_classes == 1:
        line = "{:3s}  {:8s}  {:8s}  {:8s}  {:8s}".format(
            "n", "T-prec.", "T-recall", "V-prec.", "V-recall")
    else:
        line = "{:3s}  {:8s}  {:8s}  {:8s}  {:8s}  {:8s}  {:8s}  {:8s}  {:8s}".format(
            "n", "T-prec.", "T-recall", "T-xP", "T-xR", "V-prec.", "V-recall", "V-xP", "V-xR")
    print("*****", line, flush=True)
    print(line, file=outfile, flush=True)
    i = 0
    for step, gr in cnnfiles:
        print("Evaluating CNN step {}/{} in {}".format(i, len(cnnfiles), gr), flush=True)
        i += 1
        x, model = load_CNN(gr, net.graph, image_size, num_gpus=num_gpus,
                            image_features=num_images, num_classes=num_classes)
        
        linedata = [step+1]
        for (n, imagestream) in ((n_train, imagestream_train), (n_valid, imagestream_valid)):
            #n = 25
            result = []

            print("Getting all images", flush=True)
            images, labels = imagestream.get_all_examples()
            print("Making preditions with CNN.", flush=True)
            predictions = model.predict(np.array(images), batch_size=batch_size)

            # Now we have an array with predicted images (predictions) and
            # one with expected images (labels).  We now need to calculate
            # precision and recall in parallel

            print("Processing predictions.", flush=True)
            with Pool(maxcpu) as pool:
                result = pool.starmap(evaluate_result, 
                                      zip(predictions, labels, [sampling]*len(labels)))
            
            result = np.array(result)
            precision = result[:,0].mean()
            recall = result[:,1].mean()
            if num_classes == 1:
                linedata.extend((precision, recall))
            else:
                cross_p = result[:,2].mean()
                cross_t = result[:,3].mean()
                linedata.extend((precision, recall, cross_p, cross_t))

        if num_classes == 1:
            line = "{:3d}  {:8.6f}  {:8.6f}  {:8.6f}  {:8.6f}".format(*tuple(linedata))
        else:
            line = "{:3d}  {:8.6f}  {:8.6f}  {:8.6f}  {:8.6f}  {:8.6f}  {:8.6f}  {:8.6f}  {:8.6f}".format(*tuple(linedata))
        print("*****", line, flush=True)
        print(line, file=outfile, flush=True)
        imagestream_train.reset()
        imagestream_valid.reset()
