import sys
sys.path.insert(0, '../')

import matplotlib
matplotlib.use('AGG')
import os
from datetime import datetime
from glob import glob
import numpy as np
import tensorflow as tf
import keras
from keras.utils import multi_gpu_model
from temnn.knet import net
from temnn.data.dataset import DataSet, DataEntry
from temnn.imagesimul.makeimages import MakeImages
import sys
import os
import time
import platform
import shutil
import json
import argparse

num_epochs = 5 # number of training epochs
save_epochs = 1
#loss_type = 'binary_crossentropy' # mse or binary_cross_entropy
loss_type = 'mse' # mse or binary_cross_entropy

parameters = {
    # Folder with training data
    #'data_dir': "../data/Au_cluster-110-2deg5/",
    
    # Size of the images during training (x, y)
    'image_size': (512, 512),

    # Number of classes in output of network, including the background
    # class.  Setting num_classes=1 means just a single class, no
    # background.  Otherwise, num_classes should be one higher than
    # the number of actual classes, to make room for the background
    # class.
    'num_classes': 1,

    # Image resolution range in pixels/Angstrom
    'sampling': (0.09, 0.12),

    # Defocus range in Angstrom
    'defocus': (20, 200),
    
    # Cs range in Angstrom (1 micrometer = 1e4 A)
    'Cs': (-15e4, 15e4),

    # Range of the logarithm (base 10) of the dose in electrons/A^2
    'log_dose': (2, 4.5),
    
    # Range of A22 aberration magnitude
    'a22': (0, 50),

    # Range of focal spread
    'focal_spread': (20, 40),

    # Range of blur
    'blur': (0, 3.0),

    # Range of MTF parameters (same names as in paper, except c0 which is 1.0 in paper).
    'mtf_c0': (1.0, 1.0),
    'mtf_c1': (0, 0.1),
    'mtf_c2': (0.3, 0.6),
    'mtf_c3': (2.0, 3.0),

    # normalization distance in Angstrom
    'normalizedistance': 12.0,

    # Spot size in Angstrom
    'spotsize': 0.4,
    
    # How many images to save in debug folder (None=none, True=all).
    'debug': 50,

    # If a seed to the random number generator should be used
    # Can be set to True to generate a random seed that is reused in testing run
    # or set to a specific seed that is used for reproducible training.
    # Setting it to None disable deterministic testing as well.
    'seed': [998616271, 738814483, 960996334, 235280404, 560394456],
}
    
def load(data_dir):
    
    waves = sorted(glob(data_dir+"wave/wave_*.npz"))
    points = sorted(glob(data_dir+"points/points_*.npz"))

    entries = [DataEntry(wave=w, points=p) for w,p in zip(waves,points)]
    
    return DataSet(entries)

def summary_image(y,size):
    return tf.reshape(tf.cast(tf.argmax(y,axis=3),tf.float32),(1,)+size+(1,))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("datafolder", help="The name of the folder where the data is placed.")
    parser.add_argument("folder", help="The name of the folder (in ../../../data) where the output is placed.")
    args = parser.parse_args()
    ## This script takes one argument: the name of the folder where the
    ## trained neural network is placed.
    #if len(sys.argv) >= 2:
    #    folderlabel = sys.argv[1]
    #else:
    #    print("\n\nUsage: {} foldername".format(sys.argv[0]), file=sys.stderr)
    #    sys.exit(-1)
    
    # Print on which host this is running (useful for troubleshooting on clusters).
    print("{}: Running on host '{}'".format(
        datetime.now().strftime("%Y%m%d-%H%M%S"),
        platform.node()
    ))

    # A number of output files and folders contain a timestamp as part of their name.
    folderlabel = args.folder
    timestamp = datetime.now().strftime("%Y%m%d-%H%M%S")
    summary_dir = "../" + folderlabel + "summaries/" + timestamp + "/"
    graph_path = "../" + folderlabel + '/clusters-{}.h5'
    debug_dir = "../" + folderlabel + "debug/" +  timestamp
    data_dir = args.datafolder
    #data_dir = parameters['data_dir']
    parameters['debug_dir'] = debug_dir  # Make it accessible to MakeImages

    # A seed for the random number generator may be given, for
    # deterministic runs.  In that case the valiation is made
    # deterministic too.
    #
    # A seed value of True means generate a random seed, but store it
    # so the run can be repeated in a deterministic way.
    if parameters['seed'] is True:
        parameters['seed'] = list(np.random.randint(2**30, size=5))
    if parameters['seed']:
        parameters['seed_validate'] = parameters['seed'] + [1, 42]
    else:
        parameters['seed_validate'] = None
        assert parameters['seed'] is None   # Any other false value means trouble.

    # Make folders for output
    graph_dir = os.path.dirname(graph_path)
    if graph_dir and not os.path.exists(graph_dir):
        os.makedirs(graph_dir)
    if parameters['debug']:
        os.makedirs(debug_dir)
    logfile = open(os.path.join(graph_dir, timestamp + '.log'), "wt", buffering=1)
        
    # Keep a copy of this script for reference
    shutil.copy2(__file__, graph_dir)

    # Also store the parameters in a machine_readable file
    with open(os.path.join(graph_dir, "parameters.json"), "wt") as pfile:
        json.dump(parameters, pfile, sort_keys=True, indent=4)

    # Load training data
    data = load(data_dir)

    # Determine number of GPUS
    cudavar = 'CUDA_VISIBLE_DEVICES'
    if cudavar in os.environ:
        cudadevices = os.environ[cudavar]
        numgpus = len(cudadevices.split(','))
        print(cudavar, '=', cudadevices)
        print("Found {} GPU devices".format(numgpus))
    else:
        numgpus = 1
        
    # The model is pretty big, we probably can only train on one data
    # point at a time without running out of GPU memory.
    batch_size = numgpus

    image_size = parameters['image_size'] # spatial dimensions of input/output
    image_features = 1 # depth of input data
    num_classes = parameters['num_classes'] # number of predicted class labels
    
    num_in_epoch = data.num_examples//batch_size
    num_iterations=num_epochs*num_in_epoch
    
    outputcounter = 0

    assert(batch_size % numgpus == 0)

    # We now generate a stream of training images
    imagestream = MakeImages(data, parameters, seed=parameters['seed'])

    # Create the neural network
    if numgpus > 1:
        with tf.device('/cpu:0'):
            # The master version of the model is locked onto a CPU, to
            # prevent slow GPU-GPU communication and out-of-memory
            # conditions on the hosting GPU.
            x = keras.Input(shape=image_size+(image_features,))
            serial_model = net.graph(x, output_features=num_classes)

        model = multi_gpu_model(serial_model, gpus=numgpus)
    else:
        x = keras.Input(shape=image_size+(image_features,))
        model = serial_model = net.graph(x, output_features=num_classes)

    # Here we choose the optimizer, it seems to be uncritical.
    model.compile(optimizer='rmsprop', loss=loss_type,
                  metrics=['accuracy'])
    
    if not os.path.exists(summary_dir):
        os.makedirs(summary_dir)
    
    print("Starting timing")
    before = time.time()

    # Optimize the model, saving every save_epoch epoch.
    for epoch in range(num_epochs):
        summary = None
        for i in range(num_in_epoch):
            #image,label = next_example(data)
            image,label = imagestream.next_example()
            if batch_size > 1:
                image = [image]
                label = [label]
                for b in range(1, batch_size):
                    img2, lbl2 = imagestream.next_example()
                    image.append(img2)
                    label.append(lbl2)
                image = np.concatenate(image)
                label = np.concatenate(label)

            # Train
            #y = keras.utils.to_categorical(label,2)
            #model.train_on_batch(image, y)
            h = model.train_on_batch(image, label)
            if summary is None:
                summary = np.array(h)
            else:
                summary += h

            # Print where we are
            print("Epoch: {}/{} Batch: {}/{}   [{}/{}]".format(epoch, num_epochs,
                                                               i, num_in_epoch,
                                                               (i + epoch*num_in_epoch)*batch_size,
                                                               num_iterations*batch_size),
                      flush=True)
        # Save 
        if (epoch+1) % save_epochs == 0:
            serial_model.save_weights(graph_path.format(epoch))
        print(epoch, *tuple(summary))
        print(epoch, *tuple(summary), file=logfile)
    
    totaltime = time.time() - before
    print("Time: {} sec  ({} hours)".format(totaltime, totaltime/3600))
              
