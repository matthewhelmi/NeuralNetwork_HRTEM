#!/bin/bash
### General options
#BSUB -q hpc
### -- set the job Name --
#BSUB -J mkm_AD2_all
### -- ask for number of cores (default: 1) --
#BSUB -n 1
#BSUB -R "span[hosts=1]"
### -- set walltime limit: hh:mm --  maximum 24 hours for GPU-queues right now
#BSUB -W 20:00
# request 5GB of memory
#BSUB -R "rusage[mem=3GB]"
### -- send notification at start --
##BSUB -B
### -- send notification at completion--
##BSUB -N
### -- Specify the output and error file. %J is the job-id --
### -- -o and -e mean append, -oo and -eo mean overwrite --
#BSUB -o make_expt_movie-%J.out
# -- end of LSF options --


# zapmodules

module purge
source ~/asap-pyqstem.bashrc
export PYTHONPATH=$HOME/development/structural-template-matching/build/lib.linux-x86_64-3.6:$PYTHONPATH

#for i in Anders2/Au*crop; do
#    python3 make_expt_movie.py $i
#done

python3 make_expt_movie.py Pei5/Au-CeO2_H21_1_1
python3 make_expt_movie.py Pei5/Au-CeO2_H21_1
#python3 make_expt_movie.py Pei5/Au-CeO2_O2_30_2





