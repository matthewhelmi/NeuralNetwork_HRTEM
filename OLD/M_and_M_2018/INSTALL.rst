.. _INSTALL:

==============
 Installation
==============

Installing the necessary software to run these scripts is unfortunately a bit involved at the time of writing.  It will be improves soon (hopefully!).

Currently, the instructions are assuming a Linux operating system, but the instructions work on a Mac as well if you have a Python 3 installation from Homebrew or Anaconda.


Python 3
========

Most Linux'es have Python 3 installed by now.  Usually, the ``python`` executable gives you an old Python 2, and ``python3`` gives a Python 3, but it varies, and you may have to install it.

Compilers etc
=============

You will need the GNU C/C++ compiler (g++), GNU Make and GIT.

Most Linux'es have this installed automatically, run ``g++ --version``, ``make --version`` and ``git --version`` to check that they are present.  If not, install then using the package manager of your Linux distribution.


Standard Python packages
========================

Some Linux'es come with some of these packages installed, but you can always install them for your own account using the command::

  python3 -m pip install --user PACKAGE

**Note:** ``python3 -m pip`` is just a fail-safe way of calling the right version of ``pip`` (the one for Python 3).  Python 3 is always called ``python3``, whether ``python`` is an alias for Python 2 or 3; but pip for Python 3 is sometimes called ``pip`` and sometimes ``pip3``.

The ``--user`` option instructs pip to install the packages in ``$HOME/.local``, if you mess up you can always delete that folder and start over.  If you want to keep your Python installations really clean, learn to use ``virtualenv`` - although that is **definetely** beyond the scope of this guide!

You need these packages installed:

numpy
  Efficient numeric operations.

scipy
  Scientific Python routines.

Pillow
  Modern version of the Python Imaging Library.

scikit-image
  Image handling and processing.

matplotlib
  Plotting

natsort
  "Natural" sorting of strings

tqdm
  Used by some user interfaces

Cython
  C extensions for Python.  If installed with pip, it requires setting
  $PATH as described below for `Jupyter Notebook`_.

ase
  The `Atomic Simulation Environment <https://wiki.fysik.dtu.dk/ase>`_.
  

*To install it all, do*::

  python3 -m pip install --user numpy scipy Pillow scikit-image matplotlib natsort tqdm Cython ase
  
  
Jupyter Notebook
================

Many examples are in the form of Jupyter Notebooks.  Jupyter comes with Anaconda's Python, and is also installed on many Linuxes.  If not, it is installed like any other Python package::

  python3 -m pip install --user jupyter

However, this places the executable in ``~/.local/bin`` which is normally not on your search path.  You need to add this line to your ``.bashrc`` file

::

  export PATH=$HOME/.local/bin:$PATH

To start Jupyter, go to the folder with the notebooks and run the command

::

  jupyter notebook .

TensorFlow and Keras
====================

These are the Neural Network packages.  You need to install TensorFlow with GPU support enabled (running these scripts without a GPU requires the patience of a saint).  Usually, that is just a question of pip-installing the packages ``tensorflow-gpu`` and ``Keras``::

  python3 -m pip install --user tensorflow-gpu Keras

Note that you may want to run some analysis scripts on a laptop without GPUs while running the Neural Network training and valiation on another machine.  If your laptop does not have an NVIDIA GPU, you can install the CPU-only version of tensorflow; the package is called ``tensorflow``.


Special Python packages
=======================

The scripts also depend on a few software packages developed by Jacob Madsen during his PhD.  These are not (yet) available through the standard Python package repository.  Instead, you need to download them from GitHub and compile them yourself.

PyQSTEM
-------

`PyQSTEM <https://github.com/jacobjma/PyQSTEM>`_ is Jacob Madsen's Python interface to the TEM/STEM simulation software `QSTEM <http://qstem.org>`_.  Jacob's interface is what allows us to mass-produce simulated images for training the network.

PyQSTEM requires the FFTW (Fast Fourier Transform) library; install it with your Linux' package manager.  You need the version for developers, since you will be compiling code using FFTW, it is often called ``libfftw-dev`` or something like that.

PyQSTEM itself is installed by cloning the git repository, and installing from there::

  git clone https://github.com/jacobjma/PyQSTEM.git
  cd PyQSTEM
  python3 setup.py install --user


Structural Template Matching
----------------------------

`Structural Template Matching
<https://github.com/jacobjma/structural-template-matching>`_  is a
library for finding peaks and calculating 2D stress tensors from
them.  The peak finding modules are used in these scripts to find the
positions of the atomic columns.

It is installed by cloning the git repository, and installing from
there::

  git clone https://github.com/jacobjma/structural-template-matching.git
  cd structural-template-matching
  python3 setup.py install --user

  
Getting this software (NeuralNetwork_HRTEM)
===========================================
  
This collection of scripts, modules and Notebooks is not intended to
be installed.  Clone the repository from gitlab, and run the scripts
in-place::

  git clone https://gitlab.com/schiotz/NeuralNetwork_HRTEM.git


  
