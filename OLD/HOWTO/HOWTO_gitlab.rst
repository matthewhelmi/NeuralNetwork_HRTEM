================================
How to use these files in Gitlab
================================

This is an ultra-short introduction to using git and GitLab.  It uses the command line to interface with git, several graphical programs are available, but I do not know them.

You will need `git` on your computer.  On linux, you can usually install it with the command `sudo apt install git` (on a Debian/Ubuntu like Linux).  On Windows, you can get it here: https://gitforwindows.org/


The over-simple way: Download the files
=======================================

On the GitLab project page, there is a download button (it shows a cloud) that allows you to get a .zip file with everything.  You can just use the files like this, but you will not have a way to download updates as the project evolves.  For this reason, **I recommend instead to use git to download (see "Simple use" below).**



Simple use: Get the files, get updates later
============================================

This is the simplest set-up.  You get a copy of the files in the repository, and get the ability to download changes from the main repository.  You do *not* get version control of your own changes on GitLab.  You can always start here and upgrade to the full setup later.

**Clone the repository:**

Get a copy of everything with the command::

  git clone https://gitlab.com/schiotz/NeuralNetwork_HRTEM.git

This will make a folder called `NeuralNetwork_HRTEM` with all the files in it.  If the main reposity is updated, you can get new versions of everything with the command::

  git pull

If you modify scripts it is probably a good idea to copy them first, so you do not get conflicts when you have edited them, and the files in the repository have also been changed.


The full setup:  Get files, get version control, and contribute back to the project
===================================================================================

To get the full power of GitLab, you need to register on GitLab and log in.  You can then "fork" the project, this gives you your own copy you can use to store your own changes.  If you later want to contribute them back, you do so by creating a "Merge Request".

Instead of writing a half-good guide for how to use git and gitlab in this way, I refer to the main GitLab documentation's Getting Started guide: https://docs.gitlab.com/ee/gitlab-basics/

