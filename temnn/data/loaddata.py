"""Helper functions for loading training data and neural nets."""
from .dataset import DataEntry,DataSet
from glob import glob
import os
import numpy as np

def load_training(data_dirs):
    "Load data folder (or list of data folders)."
    if isinstance(data_dirs, str):
        data_dirs = [data_dirs]
    if not data_dirs:
        raise ValueError("Empty list of folders given to load_training.")
    
    entries = []
    for data_dir in data_dirs:
        # Load abTEM wave objects
        waves=sorted(glob(os.path.join(data_dir, "wave", "wave_*")))
        # Load atomic points, columns (sites), and column heights as np arrays
        points=sorted(glob(os.path.join(data_dir, "points", "points_*.npz")))
        # Load ase atoms objects.  May have .cfg or .traj file type
        atoms=sorted(glob(os.path.join(data_dir, "model", "model_*.cfg")))
        if not len(atoms):
            atoms=sorted(glob(os.path.join(data_dir, "model", "model_*.traj")))
    
        print("Reading {} configurations from {}.".format(len(waves), data_dir))
        if not len(waves):
            raise ValueError("The folder {} contains no configurations!".format(data_dir))

        # Create list of DataEntry objects contains waves, points, and models
        entries += [DataEntry(wave=w, points=p, model=a) for w,p,a in zip(waves,points,atoms)]

    if not len(entries):
        raise ValueError("The dataset appears to be empty!")
    return DataSet(entries)

# For script compatibility
load = load_training

def find_training_folders(folder):
    """Examines a folder with training data.

    If the folder contains a 'wave' subfolder, it is a data folder and is returned.

    If not, all subfolders are tested (recursively) and any folders with
    'wave' subfolders are returned.
    """
    if os.path.isdir(os.path.join(folder, "wave")):
        return [folder]
    else:
        folders = []
        for item in os.listdir(folder):
            item = os.path.join(folder, item)
            if os.path.isdir(item) and not item.endswith('-test'):
                folders += find_training_folders(item)
        return folders
