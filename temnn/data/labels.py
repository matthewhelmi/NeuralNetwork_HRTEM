import os
import sys
import logging
import numpy as np
import itertools
import shapely.geometry as geometry
from shapely.ops import cascaded_union, polygonize
from scipy.cluster.hierarchy import linkage,fcluster
from scipy.spatial import Delaunay
from scipy.spatial.distance import cdist
from skimage import morphology, draw
from skimage.util import invert
from descartes import PolygonPatch

def project_positions(atoms,distance=1,return_counts=False):
    
    positions=atoms.get_positions()[:,:2]

    clusters = fcluster(linkage(positions), distance, criterion='distance')
    unique, indices = np.unique(clusters, return_index=True)
    positions = np.array([np.mean(positions[clusters==u] ,axis=0) for u in unique])
    counts = np.array([np.sum(clusters==u) for u in unique])
    
    if return_counts:
        return positions, counts
    else:
        return positions

def create_label(positions,shape,label,num_classes=None,classes=None,width=None,null_class=False):
    valid_labels = {'Mask', 'Blob', 'Disk', 'Gaussian'} 
    ## Check peak shape
    if label not in valid_labels:
        raise ValueError("Labels must be one of {}.".format(valid_peaks))
    
    if classes is None:
        if label == 'Disk' or label == 'Gaussian':
            classes=[0]*len(positions)
    if num_classes is None:
        num_classes = np.max(classes)+1
 
    # Initialise labels array
    if null_class:
        labels=np.zeros(shape+(num_classes-1,))
    else:
        labels=np.zeros(shape+(num_classes,))
   
    # Semantic segmentation of nanoparticles/flakes 
    if label == 'Mask':
        # If a list of multiple atomic configurations are given, this calls for
        # multi-class segmentation (ex. nanoparticles and substrate). So for 
        # generalisability I will work with lists of separate arrays of 
        # positions. I therefore require that the number of arrays contained in 
        # this list matches the desired number of class. I will not use a 
        # background class.
        if not isinstance(positions,list):
            positions = [positions]
        # Extract points as shapely.geometry.Point
        for c, pos in enumerate(positions): #c for classes
            points = []
            for p in pos:
                points.append(geometry.shape(geometry.Point(p[0],p[1])))

            # Concave hull
            # Alpha is hardcoded to 0 for convex hull
            alpha=0
            #alpha=optimizealpha(points)
            #print('alpha: ', alpha)
            concave_hull = alphashape(points,alpha=alpha)
            #concave_hull = alphashape(points,alpha=0.01)
            if (concave_hull.is_empty or
               isinstance(concave_hull, geometry.MultiPolygon)
               ):
                alpha=optimizealpha(points)
                concave_hull = alphashape(points,alpha=alpha)

            # Create mask from concave hull
            exterior_pts = np.asarray(list(zip(*concave_hull.exterior.coords.xy)))
            mask = draw.polygon2mask(shape, exterior_pts)

            # Combine exterior discs with concave hull mask
            labels[:,:,c] += mask

        # Fill in empty classes (nanoparticles with no substrate for example)        
        if (num_classes - len(positions)) > 1:
            for c in np.arange(len(positions),num_classes-1):
                labels[:,:,c] = np.zeros(shape)

            # Add background class as the last
            bg = invert(np.sum(labels,axis=-1))
            labels[:,:,num_classes-1] = bg
        
        labels[labels > 1.0] = 1.0   

    # Gaussian blob object localisation
    if label == 'Blob':
        # Fix Gaussian blob at centroid of points
        points = positions[:,:2]
        x = [p[0] for p in points]
        y = [p[1] for p in points]
        centroid = (sum(x) / len(x), sum(y) / len(y))
        # Initializing value of x-axis and y-axis
        x,y = np.mgrid[0:shape[0],0:shape[1]]

        # Calculating Gaussian array
        labels[:,:,0] = np.exp(-(((x-centroid[0])**2/(2.0 * width**2))+((y-centroid[1])**2/(2.0 * width**2))))

    # Atomic scale segmentation of nanoparticles/flakes using gaussian peaks
    if label == 'Gaussian':
        x,y = np.mgrid[0:shape[0],0:shape[1]]
        for p,c in zip(positions,classes):
            p_round=np.round(p).astype(int)
            min_xi = np.max((p_round[0]-width*4,0))
            max_xi = np.min((p_round[0]+width*4+1,shape[0]))
            min_yi = np.max((p_round[1]-width*4,0))
            max_yi = np.min((p_round[1]+width*4+1,shape[1]))
            
            xi = x[min_xi:max_xi,min_yi:max_yi]
            yi = y[min_xi:max_xi,min_yi:max_yi]
            v=np.array([xi.ravel(),yi.ravel()])
            
            labels[xi,yi,c]+=np.exp(-cdist([p],v.T)**2/(2*width**2)).reshape(xi.shape)
 
    # Atomic scale segmentation of nanoparticles/flakes using binary discs
    if label == 'Disk':
        x,y=np.mgrid[0:shape[0],0:shape[1]]
        first = True
        for p,c in zip(positions,classes):
            p_round=np.round(p).astype(int)
            if first:
                width = int(width)
                first = False
            min_xi = np.max((p_round[0]-width,0))
            max_xi = np.min((p_round[0]+width+1,shape[0]))
            min_yi = np.max((p_round[1]-width,0))
            max_yi = np.min((p_round[1]+width+1,shape[1]))
            
            xi = x[min_xi:max_xi,min_yi:max_yi]
            yi = y[min_xi:max_xi,min_yi:max_yi]
            
            #labels[xi,yi,c] += morphology.disk(width)
            labels[xi,yi,c] = np.maximum(labels[xi,yi,c],morphology.disk(width))

    if null_class:
        labels=np.concatenate((labels,1-np.sum(labels,axis=2).reshape(labels.shape[:2]+(1,))),axis=2)
    
    labels = labels.clip(0.0,1.0)
    return labels

def alphashape(points, alpha=None):
    """
    Compute the alpha shape (concave hull) of a set of points.  If the number
    of points in the input is three or less, the convex hull is returned to the
    user.  For two points, the convex hull collapses to a `LineString`; for one
    point, a `Point`.
    Args:
      points (list or ``shapely.geometry.MultiPoint``
      alpha (float): alpha value
    Returns:
      ``shapely.geometry.Polygon`` or ``shapely.geometry.LineString`` or
      ``shapely.geometry.Point``
          the resulting geometry

    Credits: https://github.com/bellockk/alphashape/tree/v1.3.1
    """
    if not isinstance(points, geometry.MultiPoint):
        points = geometry.MultiPoint(list(points))

    # If given a triangle for input, or an alpha value of zero or less,
    # return the convex hull.
    if len(points.geoms) < 4 or (alpha is not None and alpha <= 0):
        result = points.convex_hull
        return result

    # Determine alpha parameter if one is not given
    if alpha is None:
        alpha=optimizealpha(points)

    coords = np.array([point.coords[0] for point in points])
    tri = Delaunay(coords)
    edges = set()
    edge_points = []

    # Loop over triangles
    for ia, ib, ic in tri.vertices:
        pa = coords[ia]
        pb = coords[ib]
        pc = coords[ic]

        # Lengths of sides of triangle
        a = np.sqrt((pa[0] - pb[0])**2 + (pa[1] - pb[1])**2)
        b = np.sqrt((pb[0] - pc[0])**2 + (pb[1] - pc[1])**2)
        c = np.sqrt((pc[0] - pa[0])**2 + (pc[1] - pa[1])**2)

        # Semiperimeter of triangle
        s = (a + b + c) * 0.5

        # Area of triangle by Heron's formula
        # Precompute value inside square root to avoid unbound math error in
        # case of 0 area triangles.
        area = s * (s - a) * (s - b) * (s - c)

        if area > 0:
            area = np.sqrt(area)

            # Radius Filter
            if a * b * c / (4.0 * area) < 1.0 / alpha:
                for i, j in itertools.combinations([ia, ib, ic], r=2):
                    if (i, j) not in edges and (j, i) not in edges:
                        edges.add((i, j))
                        edge_points.append(coords[[i, j]])

    # Create the resulting polygon from the edge points
    m = geometry.MultiLineString(edge_points)
    triangles = list(polygonize(m))
    result = cascaded_union(triangles)

    return result

def _testalpha(points, alpha):
    """
    Evaluates an alpha parameter.
    This helper function creates an alpha shape with the given points and alpha
    parameter.  It then checks that the produced shape is a Polygon and that it
    intersects all the input points.
    Args:
        points (``shapely.geometry.Multipoint``): data points
        alpha (float): alpha value
    Returns:
        bool: True if the resulting alpha shape is a single polygon that
            intersects all the input data points.

    Credits: https://github.com/bellockk/alphashape/tree/v1.3.1
    """
    polygon = alphashape(points, alpha)
    if isinstance(polygon, geometry.polygon.Polygon) and all(
            [polygon.intersects(point) for point in points]):
        return True
    else:
        return False

def optimizealpha(points, max_iterations: int = 10000, lower: float = 0.,
                  upper: float = sys.float_info.max, silent: bool = False):
    """
    Solve for the alpha parameter.
    Attempt to determine the alpha parameter that best wraps the given set of
    points in one polygon without dropping any points.
    Note:  If the solver fails to find a solution, a value of zero will be
    returned, which when used with the alphashape function will safely return a
    convex hull around the points.
    Args:
        points (list): an iterable container of points
        max_iterations (int): maximum number of iterations while finding the
            solution
        lower (float): lower limit for optimization
        upper (float): upper limit for optimization
        silent (bool): silence warnings
    Returns:
        float: The optimized alpha parameter
    
    Credits: https://github.com/bellockk/alphashape/tree/v1.3.1
    """
    points = geometry.MultiPoint(list(points))

    # Set the bounds
    assert lower >= 0, "The lower bounds must be at least 0"
    # Ensure the upper limit bounds the solution
    assert upper <= sys.float_info.max, (
        f'The upper bounds must be less than or equal to {sys.float_info.max} '
        'on your system')

    if _testalpha(points, upper):
        if not silent:
            logging.error('the max float value does not bound the alpha '
                          'parameter solution')
        return 0.

    # Begin the bisection loop
    counter = 0
    while (upper - lower) > np.finfo(float).eps * 2:
        # Bisect the current bounds
        test_alpha = (upper + lower) * .5

        # Update the bounds to include the solution space
        if _testalpha(points, test_alpha):
            lower = test_alpha
        else:
            upper = test_alpha

        # Handle exceeding maximum allowed number of iterations
        counter += 1
        if counter > max_iterations:
            if not silent:
                logging.warning('maximum allowed iterations reached while '
                                'optimizing the alpha parameter')
            lower = 0.
            break

    return lower
