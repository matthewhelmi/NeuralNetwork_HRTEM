"""Class for randomness"""
import numpy as np
class RandomScale:
    def __init__(self, rnd):
        self.rnd = rnd
        self.n = 0
    def __call__(self, low=0.0, high=1.0):
        r = self.rnd[self.n]
        self.n += 1
        return (high - low) * r + low
    def randint(self, low, high):
        r = self()
        ri = int(np.floor((high - low) * r) + low)
        assert low <= ri < high
        return ri

