"Functions for generating and viewing images"
import os
import numpy as np
import matplotlib.pyplot as plt
from abtem import CTF
from mpl_toolkits.axes_grid1 import make_axes_locatable
from .randomscale import RandomScale
from temnn.data.dataset import DataEntry
from scipy import fft
import json
from stm.feature.peaks import find_local_peaks

def show_example(image, label, text, filename, labeltype):
    """
    Save a png with image and label
    
    Parameters:
    -----------
    image: numpy array
        The generated TEM image
    label:  numpy array
        The geneated labels (ground truth)
    text: string
        Output text of relevant parameters
    filename: string
        Output path
    """
    channels = image.shape[-1]
    out_channels = label.shape[-1]
    width = 24.0
    fig = plt.figure(figsize=(width,width/(channels+out_channels)))

    for i in range(channels):
        ax = fig.add_subplot(1, channels+out_channels, i+1)

        im = ax.imshow(image[0,:,:,i].T, origin='lower', interpolation='nearest', cmap='gray')
        divider = make_axes_locatable(ax)
        cax1 = divider.append_axes("right", size="5%", pad=0.05)
        cbar = plt.colorbar(im, cax = cax1)
    for j in range(out_channels):
        ax = fig.add_subplot(1, channels+out_channels, channels+j+1)

        if labeltype == "Exitwave" or labeltype == "Exitwave_no_support":
            vmax = max(-label[0].min(), label[0].max())
            extraargs = dict(cmap="seismic", vmin=-vmax, vmax=vmax)
        else:
            extraargs = {}
        im = ax.imshow(label[0,:,:,j].T, origin='lower', interpolation='nearest', **extraargs)
        divider = make_axes_locatable(ax)
        cax2 = divider.append_axes("right", size="5%", pad=0.05)
        cbar = plt.colorbar(im, cax = cax2)

    plt.tight_layout()
    fig.savefig(filename+'.png', bbox_inches='tight')
    with open(filename+'.txt', "wt") as info:
        info.write(text)
    plt.close(fig)

def tem_parameters(params,rndnums):
    """
        Genarates the random TEM imaging parameters.
    """
    ### *** Set up randonmness ***
    rnd = RandomScale(rndnums)

    ### *** Define values ***
    tem_params = {
        'sampling': rnd(*params['sampling']),
        ## CTF parameters
        'defocus': rnd(*params['defocus']),
        'ctf_c30': rnd(*params['ctf_c30']),
        'ctf_c12': rnd(*params['ctf_c12']),
        'ctf_phi12': rnd(0, 2*np.pi),
        'ctf_c21':  rnd(*params['ctf_c21']),
        'ctf_phi21': rnd(0, 2*np.pi),
        'ctf_c23': rnd(*params['ctf_c23']),
        'ctf_phi23': rnd(0, 2*np.pi),
        'ctf_c32': rnd(*params['ctf_c32']),
        'ctf_phi32': rnd(0, 2*np.pi),
        'ctf_c34': rnd(*params['ctf_c34']),
        'ctf_phi34': rnd(0, 2*np.pi),
        'ctf_c45': rnd(*params['ctf_c45']),
        'ctf_phi45': rnd(0, 2*np.pi),
        'focal_spread': rnd(*params['focal_spread']),
        ## Noise parameters
        'dose': 10**rnd(*params['log_dose']),
        'blur': rnd(*params['blur']),
        ## MTF parameters
        #'mtf_c0': rnd(*params['mtf_c0']),
        'mtf_c1': rnd(*params['mtf_c1']),
        'mtf_c2': rnd(*params['mtf_c2'])#,
        #'mtf_c3': rnd(*params['mtf_c3'])
    }

    return tem_params, rnd

def correlation(x,c,d,e,f):
    return (d-e)*(1/(1 + (x/c)**f))+e

def makeimage(entry, params, imgnum, rndnums, label):
    """
    Make a TEM image.

    entry: A data entry containing at least an exit wave function.

    size:  Size of desired image in pixels (2-tuple).

    imgnum: Sequential number used when plotting in debug mode.

    rndnums: XX random numbers (uniformly in [0;1[).  This prevents
             trouble with random numbers when multiprocessing.

    label: Specify the type of label for the image:
           'Mask', 'Blob', 'Gaussian', 'Disk', 'Exitwave'
    """
    #######################################################
    ### *** Generate a set of random TEM parameters *** ###
    #######################################################
    tem_params, rnd = tem_parameters(params,rndnums)
    if params['mtf'] == 'lorentzian_correlated':
        import random
        a = params['mtf_c1'][0]
        b = params['mtf_c1'][1]
        tem_params['mtf_c1'] = a*tem_params['mtf_c2']+b + random.uniform(-0.01,0.01)
    if params['mtf'] == 'lorentzian_extra_correlated':
        import random
        tem_params['mtf_c2'] = correlation(tem_params['dose'], *params['mtf_correlation'])

        a = params['mtf_c1'][0]
        b = params['mtf_c1'][1]
        tem_params['mtf_c1'] = a*tem_params['mtf_c2']+b + random.uniform(-0.01,0.01)
    if params['mtf'] == 'parametric':
        tem_params['mtf_c3'] = rnd(*params['mtf_c3'])
    if params['readout'] is not None:
        tem_params['readout'] = rnd(*params['readout'])
    else: # set to none
        tem_params['readout'] = params['readout']    
    
    ##############################
    ### *** Extract values *** ###
    ##############################
    ######################
    ## Image parameters ##
    ######################
    # Image dimensions
    size = params['image_size']
    # Image sampling
    sampling = tem_params['sampling']
   
    ######################
    ## Label parameters ##
    ######################
    # Size of spots for atomic column labels - normally 0.4
    spotsize = params['spotsize']
 
    ###########################
    ## Microscope parameters ##
    ###########################
    # Contrast transfer function values
    ctf_params = {
        'C30': tem_params['ctf_c30'],
        'C12': tem_params['ctf_c12'], 'phi12': tem_params['ctf_phi12'],
        'C21': tem_params['ctf_c21'], 'phi21': tem_params['ctf_phi21'],
        'C23': tem_params['ctf_c23'], 'phi23': tem_params['ctf_phi23'],
        'C32': tem_params['ctf_c32'], 'phi32': tem_params['ctf_phi32'],
        'C34': tem_params['ctf_c34'], 'phi34': tem_params['ctf_phi34'],
        'C45': tem_params['ctf_c45'], 'phi45': tem_params['ctf_phi45'] 
    }
    # Focal spread
    focal_spread = tem_params['focal_spread']
    # Electron dose
    dose = tem_params['dose']
    # Gaussian blur
    blur = tem_params['blur']
    # Modulation transfer function values
    if params['mtf'] == 'parametric':
        mtf_params = {
            'c1': tem_params['mtf_c1'],
            'c2': tem_params['mtf_c2'],
            'c3': tem_params['mtf_c3']
            }
    else:
        mtf_params = {
            'c1': tem_params['mtf_c1'],
            'c2': tem_params['mtf_c2']
            }

    # Generate a series of images at different focus settings
    if params.get('multifocus') is None:
        multifocus = False
        defocuses = [tem_params['defocus'],]
    else:
        multifocus, deltafocus, deltadeltafocus = params['multifocus']
        defocuses = [tem_params['defocus'],]
        for i in range(1, multifocus):
            defocuses.append(defocuses[-1] + rnd(deltafocus-deltadeltafocus,
                                             deltafocus+deltadeltafocus))
        tem_params['defocus'] = defocuses
 
    ## Debugging options
    # Ouput examples to debug folder
    debug = params['debug']
    debug_dir = params['debug_dir']
    
    #########################################
    ### *** Create Images and Labels *** ####
    #########################################
    # Load waves, atom sites, atom positions, and atom objects as
    # Dataset object
    entry.load()
    tem_params['energy'] = entry._wave.energy

    # Initialise CTF and generate image
    for i, defocus in enumerate(defocuses):
        #ctf_params['C10'] = -defocus
        ctf = CTF(defocus=defocus,
                  energy=entry._wave.energy,
                  focal_spread=focal_spread,
                  parameters=ctf_params
        )
        entry.create_image(ctf,
                           sampling,
                           blur,dose,
                           params['noise'],
                           params['mtf'],
                           mtf_params,
                           tem_params['readout'],
                           concatenate=(i>0)
        )
    
    ##############################
    ###### Generate label ########
    ##############################
    # We can do Nanoparticle segmentation with Masks or blobs (pseudo-object localisaiton)
    # or we can do atomic segmetnation with discs or gaussian peaks. Masks do not require a width,
    # and the blob with will be calculated based on positions.
    assert len(size) == 2 and size[0] == size[1]
    newsampling = sampling * (size[1] - 1) / size[1]
    if label == 'Mask' or label == 'Blob':
        entry.create_label(newsampling,
                       label=label,
                       null_class=params['null_class'],
                       num_classes=params['num_classes'])
    elif label == 'Exitwave':
        entry.create_label(sampling,
                           width=int(spotsize/sampling),
                           label=label,
                           num_classes=params['num_classes'])
    elif label == 'Exitwave_no_support':
        wave_file_name=entry.wave_file.replace('wave/','wave_no_support/')
        entry2= DataEntry(wave_file_name,entry.points_file)
        entry2.load()
        entry2._image = entry._image
        entry2.create_label(sampling,
                           width=int(spotsize/sampling),
                           label='Exitwave',
                           num_classes=params['num_classes'])
        entry._label=entry2._label
    else: # Disk or Gaussian atomic column segmentation, containing background(null) class
        entry.create_label(newsampling,
                           width=int(spotsize/newsampling),
                           label=label,
                           null_class=params['null_class'],
                           num_classes=params['num_classes'])

    if 'FFT_filter_lat_const' in params:
        # If FFT filtering is specified in the parameter file, identify
        # hexagonal peaks with the specified lattice constant, and remove
        # peaks with the three lowest order.
        latconst = params['FFT_filter_lat_const']
        for layer in range(entry._image.shape[3]):
            image = entry._image[0,:,:,layer]
            filtered_image = hexagonal_fourier_filter(image, latconst, sampling)
            entry._image[0,:,:,layer] = filtered_image         
        
    # Image dimensions 
    shape = entry._image.shape[1:3]
    assert not ((size[0] > shape[0]) != (size[1] > shape[1]))
    if shape[0] > size[1]:
        assert shape[1] >= size[1]
        amount = shape[1] - size[1]
        entry.crop(amount, amount, sampling)
    elif shape[0] < size[1]:
        assert shape[1] <= size[1]
        entry.pad(size)
    else:
        assert shape[1] == size[1]

    # Apply random image alterations        
    image, lbl=entry.as_tensors()
    entry.reset()

    ## Output for debugging
    if debug is True or imgnum < debug:
        if params['mtf'] == 'parametric':
            text = (f"sampling={sampling}\n"
                    f"focal_spread={focal_spread}\n" 
                    f"defocus={tem_params['defocus']}\n" 
                    f"energy={tem_params['energy']}\n" 
                    f"C30={tem_params['ctf_c30']}\n"
                    f"C12={tem_params['ctf_c12']}\n"
                    f"phi12={tem_params['ctf_phi12']}\n"
                    f"C21={tem_params['ctf_c21']}\n"
                    f"phi21={tem_params['ctf_phi21']}\n"
                    f"C23={tem_params['ctf_c23']}\n"
                    f"phi23={tem_params['ctf_phi23']}\n"
                    f"C32={tem_params['ctf_c32']}\n"
                    f"phi32={tem_params['ctf_phi32']}\n"
                    f"C34={tem_params['ctf_c34']}\n"
                    f"phi34={tem_params['ctf_phi34']}\n"
                    f"C45={tem_params['ctf_c45']}\n"
                    f"phi45={tem_params['ctf_phi45']}\n"
                    #f"mtf0={mtf_params['c0']}\n"
                    f"mtf1={mtf_params['c1']}\n"
                    f"mtf2={mtf_params['c2']}\n"
                    f"mtf3={mtf_params['c3']}\n"
                    f"N0={tem_params['readout']}\n"
                    f"dose={dose}\n"
                    f"blur={blur}"
                    )
        else:
            text = (f"sampling={sampling}\n"
                    f"focal_spread={focal_spread}\n" 
                    f"defocus={tem_params['defocus']}\n" 
                    f"energy={tem_params['energy']}\n" 
                    f"C30={tem_params['ctf_c30']}\n"
                    f"C12={tem_params['ctf_c12']}\n"
                    f"phi12={tem_params['ctf_phi12']}\n"
                    f"C21={tem_params['ctf_c21']}\n"
                    f"phi21={tem_params['ctf_phi21']}\n"
                    f"C23={tem_params['ctf_c23']}\n"
                    f"phi23={tem_params['ctf_phi23']}\n"
                    f"C32={tem_params['ctf_c32']}\n"
                    f"phi32={tem_params['ctf_phi32']}\n"
                    f"C34={tem_params['ctf_c34']}\n"
                    f"phi34={tem_params['ctf_phi34']}\n"
                    f"C45={tem_params['ctf_c45']}\n"
                    f"phi45={tem_params['ctf_phi45']}\n"
                    #f"mtf0={mtf_params['c0']}\n"
                    f"mtf1={mtf_params['c1']}\n"
                    f"mtf2={mtf_params['c2']}\n"
                    f"N0={tem_params['readout']}\n"
                    f"dose={dose}\n"
                    f"blur={blur}"
                    )
        fn = os.path.join(debug_dir, "img-{}".format(imgnum))
        show_example(image, lbl, text, fn, label)
            
    return image,lbl,tem_params

# Help functions for FFT-filtering

def hexagonal_fourier_filter(image, latconst, sampling):
    """Remove spatial frequencies from a hexagonal lattice.

    image: The image to process
    latconst: The lattice constant of the lattice to remove
    sampling: The pixel size.
    """
    
    nx,ny=image.shape
    assert nx == ny
    
    # Lattice constant per sampling
    lc_samp = latconst/sampling
        
    # spread
    peakedge = 0.2 / sampling  # Smoothness of peaks
    peaksize = 0.3 / sampling  # Size of peaks
    ringwidth = 0.1 * latconst / sampling
    
    # pixel matrix centered around 0
    x = np.arange(nx) - nx//2
    
    # filter: A ring containing the first peaks of the support
    fft_C = ringfilter(x[:,np.newaxis], x[np.newaxis,:], lc_samp, nx, ringwidth)
    # FFT transformed images
    fftimg_C = fft.fft2(image)
    fftimg_C[0,0] = 0
    shifted = fft.fftshift(fftimg_C)
    
    # rings to find the peaks in
    ring_peaks = shifted * fft_C
    signal = np.abs(ring_peaks)
    signal /= signal.max()
    
    # the peaks in the ring
    peaks = find_local_peaks(signal, min_distance=20,threshold=0.85,exclude_adjacent=True).astype(int)
    
    # the center of the array
    center = np.array([nx/2,ny/2])
    # distance from center to peak
    v = peaks[0]-center
    
    # first 6 peaks in lattice in carbon; this will have to be changed
    # based on the geometry of the sought after removed background
    peaks_1st_ring = np.array([ np.dot(rot(a), v) + center for a in np.pi/3 * np.arange(6) ])
    peaks_2nd_ring = np.array([ np.dot(rot(a), v * np.sqrt(3)) + center for a in np.pi/6 + np.pi/3 * np.arange(6)])
    peaks_3rd_ring = np.array([ np.dot(rot(a), v * 2) + center for a in np.pi/3 * np.arange(6)])
    v_peak = np.concatenate((peaks_1st_ring, peaks_2nd_ring, peaks_3rd_ring), axis=0)
    
    fftspot = spotfilters(v_peak, peaksize=peaksize, img_size=(nx,ny), alpha=peakedge)
    shift_img_no_peak = shifted * fftspot
    fft_img_no_peak = fft.ifftshift(shift_img_no_peak)

    return np.real(fft.ifft2(fft_img_no_peak))


def rot(a):
    return np.array([[np.cos(a),-np.sin(a)],[np.sin(a),np.cos(a)]])

def fermi(x, beta):
    beta_x = np.clip(beta * x, -100., 100.)
    return 1 / (np.exp(beta_x) + 1)

def ringfilter(x, y, latconst, nx, width):
    """Return a fourier filter that keeps only a ring in Fourier space.

    The ring returned correspond to a specified lattice constant, has
    a specified width, and smooth edges with a smoothness of 20% of the width.

    latconst: Lattice constant (in pixels).

    nx: Number of pixels in array returned.

    width: width of ring (in pixels).
    """
    x0 = np.arange(nx) - nx//2
    x = x0[:,np.newaxis]
    y = x0[np.newaxis,:]
    a = nx * (2 / np.sqrt(3)) / (latconst)
    a0 = a - width
    a1 = a + width
    beta = 1 / (0.2 * width )
    r = np.sqrt(x**2 + y**2)
    return fermi(r - a1, beta) - fermi(r - a0, beta)

def spotfilter(peak, peaksize, img_size, alpha):
    x=np.arange(img_size[0])-peak[0]
    y=np.arange(img_size[1])-peak[1]   
    x=x[:,np.newaxis]
    y=y[np.newaxis,:]
    a = peaksize
    beta = 1 / (0.2 * alpha )
    r = np.sqrt(x**2 + y**2)
    return fermi(a-r, beta)

def spotfilters(peaks, peaksize,img_size, alpha):
    results = spotfilter(peaks[0],peaksize,img_size, alpha)
    for p in peaks[1:]:
        results *= spotfilter(p,peaksize,img_size, alpha)
    return results
