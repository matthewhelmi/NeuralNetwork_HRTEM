from tensorflow import pad
from tensorflow.keras.layers import Layer
import math

'''
  2D Reflection Padding
  Attributes:
    - padding: (padding_width, padding_height) tuple
'''
class ReflectionPadding2D(Layer):
    def __init__(self, padding=(1, 1), **kwargs):
        self.padding = tuple(padding)
        super(ReflectionPadding2D, self).__init__(**kwargs)

    def compute_output_shape(self, input_shape):
        return (input_shape[0], input_shape[1] + 2 * self.padding[0], input_shape[2] + 2 * self.padding[1], input_shape[3])

    def call(self, input_tensor, mask=None):
        padding_height, padding_width = self.padding
        if padding_height % 2 != 0:
            pad_top = pad_left = math.floor(padding_height / 2) 
            pad_bottom = pad_right = math.ceil(padding_height / 2)
        elif padding_height % 2 == 0:
            pad_top = pad_left = pad_bottom = pad_right = int(padding_height / 2)
        return pad(input_tensor, [[0,0], [pad_top, pad_bottom], [pad_left, pad_right], [0,0] ], 'REFLECT')

    # Allow TF to save the model since we are making a custom layer
    def get_config(self):
        config = super().get_config().copy()
        config.update({
            'padding': self.padding
        })
        return config
