from __future__ import absolute_import, print_function

import tensorflow as tf
from tensorflow.keras.models import Model
from tensorflow.keras import layers, regularizers
#from tensorflow import name_scope
from .upsampling import BilinearUpSampling2D
#from tensorflow.keras.layers.advanced_activations import PReLU
from tensorflow.keras import initializers
from .ReflectionPadding2D import ReflectionPadding2D

weight_decay = None

def graph(x, output_features, channels=32, kernel_size=[9,9],
            reflection_padding=True):
    """Define the Convolutional Neural Network, a U-Net.

    Parameters:

    output_features: The number of output feature channels (including an
    eventual background channel).

    channels: The number of feature channels in the high-resolution part
    of the U-Net.
    """
    
    down1 = conv_res_conv(x, channels, kernel_size, 
                            reflection_padding, name="down1")
    pool1 = pool_layer(down1, name="pool1")
    
    down2 = conv_res_conv(pool1, channels*2, kernel_size, 
                            reflection_padding, name="down2")
    pool2 = pool_layer(down2, name="pool2")
    
    down3 = conv_res_conv(pool2, channels*4, kernel_size,
                             reflection_padding, name="down3")
    pool3 = pool_layer(down3, name="pool3")

    bridge = conv_res_conv(pool3, channels*8, kernel_size,
                            reflection_padding, name="bridge")

    up3 = upsample_layer(bridge, channels*4, name="up3")
    up3 = skip(up3, down3, name='skip3')
    up3 = conv_res_conv(up3, channels*4, kernel_size,
                            reflection_padding, name="up3")
    
    up2 = upsample_layer(up3, channels*2, name="up2")
    up2 = skip(up2, down2, name='skip2')
    up2 = conv_res_conv(up2, channels*2, kernel_size,
                            reflection_padding, name="up2")
    
    up1 = upsample_layer(up2, channels, name="up1")
    up1 = skip(up1, down1, name='skip1')
    up1 = conv_res_conv(up1, channels, kernel_size,
                        reflection_padding, name="up1")

    inference = score_layer(up1, channels=output_features)
    return Model(x, inference)

def conv_res_conv(x, channels, kernel_size, 
                    reflection_padding, name='conv_res_conv'):
    """Define a block in the network.

    The block consists of a convolutional layer, a residual block, and
    a convolutional layer.
    """
    x = conv_layer(x, channels, kernel_size,
                    reflection_padding, name=name+"/conv1")
    x = res_block(x, channels, kernel_size,
                    reflection_padding, name=name+"/resid")
    return conv_layer(x, channels, kernel_size, 
                        reflection_padding, name=name+"/conv2")

def res_block(x, channels, kernel_size,
                reflection_padding, name='res_block'):
    "A residual block."
    y = conv_layer(x, channels, kernel_size, 
                    reflection_padding, name=name+'/conv_1')
    y = conv_layer(y, channels, kernel_size,
                    reflection_padding, name=name+'/conv_2')
    y = conv_layer(y, channels, kernel_size,
                    reflection_padding, name=name+'/conv_3')
    return skip(x, y, name=name+'/add')

def conv_layer(x, channels, kernel_size, 
                reflection_padding, name='conv'):
    """A single convolutional layer."""
    if reflection_padding == True:
        # Reflection Padding
        if isinstance(kernel_size, list):
            # incase kernel_size is given as a list [x,y]
            x = ReflectionPadding2D(padding=(kernel_size[0]-1,
                                        kernel_size[1]-1))(x)
        elif isinstance(kernel_size, int):
            # otherwise all good
            x = ReflectionPadding2D(padding=(kernel_size-1,
                                        kernel_size-1))(x)
        padding = 'valid'
    else:
        padding = 'same'
    conv = layers.Conv2D(channels, kernel_size, padding=padding,
                             kernel_regularizer=regul(),
                             kernel_initializer='RandomNormal',
                             bias_initializer=get_bias_init(),
                             name=name)
    x = prelu(conv(x), name=name)
    # Normalize
    return layers.BatchNormalization(name=name+'_b_norm')(x)

def skip(x, y, name):
    return layers.concatenate([x, y], name=name)

def pool_layer(x, name='pool'):
    return layers.MaxPooling2D(pool_size=2, padding='same', name=name)(x)

# Bilinear upsampling.
def upsample_layer(x, channels, name='upsample'):
    #x = layers.UpSampling2D(size=2, name=name+'/upsamp2D')(x)
    x = BilinearUpSampling2D(name=name+'/upsamp2D')(x)
    return conv_layer(x, channels, kernel_size=1,
                        reflection_padding=False, name=name+'/up_conv')


def score_layer(x, channels, name="score"):
    "The final layer."
    if channels > 1:
        act = 'softmax'
    else:
        act = 'sigmoid'
    conv = layers.Conv2D(channels, kernel_size=1,
                             activation=act,
                             padding='same',
                             kernel_regularizer=regul(),
                             kernel_initializer='RandomNormal',
                             bias_initializer=get_bias_init(),
                             name=name)
    return conv(x)

# Regularization
def regul():
    if weight_decay:
        return regularizers.l2(weight_decay)
    else:
        return None

def get_bias_init():
    return initializers.Constant(0.1)

# Parametric RELU activiation
def prelu(x, name=None):
    p = layers.PReLU(shared_axes=[1,2], name=name+"/PReLU",
                        alpha_initializer=initializers.Constant(0.01))
    return p(x)
