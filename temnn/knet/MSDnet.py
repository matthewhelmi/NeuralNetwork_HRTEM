from __future__ import absolute_import, print_function

import tensorflow_addons as tfa
from tensorflow.keras.models import Model
from tensorflow.keras import layers, regularizers
#from tensorflow import name_scope
#from tensorflow.keras.layers.advanced_activations import PReLU
from tensorflow.keras import initializers
from .ReflectionPadding2D import ReflectionPadding2D
import numpy as np

weight_decay = None

def graph(x, output_features, channels=3, layers=50,
        kernel_size=[9,9], reflection_padding=True,
        dilation_rate_limit=10, random_dil=True, seed=None):
    """Define the Convolutional Neural Network, an MSD-Net.

    Parameters:

    output_features: The number of output feature channels (including an
    eventual background channel).

    channels: The number of feature channels in the MSD-Net.

    layers = Number of layers in the MSD-net

    dilation_rate_limit: Max dilation rate

    random_dil: Set the dilation rate randomly or iteratively
    """
    limit = dilation_rate_limit
    if random_dil:
        # Dilations are random between 1 and limit
        if seed is None:
            seed = 1234
        rng = np.random.default_rng(seed)
    else:
        # Dilations will iteratively increase till limit 
        # and then reset to 1
        dilarr = np.arange(1,limit+1)
    # Input for layers
    y = x
    if not random_dil:
        dil = 0
    for i in range(layers):
        if random_dil:
            rnd_dil = rng.integers(1,limit+1,size=1)
            dilation_rate = [rnd_dil[0], rnd_dil[0]]
        else:
            dilation_rate = [dilarr[dil], dilarr[dil]]
        # Construct a layer of the MSD-net
        convol = conv_layer(y, channels, dilation_rate,
                            kernel_size, reflection_padding,
                            name="conv{}".format(i+1)) 
        concat = conc(y, convol, name="conc{}".format(i+1))
        # Input to next layer
        y = concat
        # If necessary reset dilation iterator
        if not random_dil:
            dil += 1
            if dil >= len(dilarr):
                dil = 0
    
    inference = score_layer(y, channels=output_features)
    return Model(x, inference)
  
def conv_layer(x, channels, dilation_rate, kernel_size,
                reflection_padding, name='conv'):
    """A single convolutional layer."""
    if reflection_padding == True:
        # Reflection Padding
        if isinstance(kernel_size, list):
            # incase kernel_size is given as a list [x,y]
            x = ReflectionPadding2D(padding=(dilation_rate[0]*(kernel_size[0]-1),
                                        dilation_rate[1]*(kernel_size[1]-1)))(x)
        elif isinstance(kernel_size, int):
            # otherwise all good
            x = ReflectionPadding2D(padding=(dilation_rate*(kernel_size-1),
                                        dilation_rate*(kernel_size-1)))(x)
        padding = 'valid'
    else:
        padding = 'same'
    conv = layers.Conv2D(channels, kernel_size, padding=padding,
                             dilation_rate=dilation_rate,
                             kernel_regularizer=regul(),
                             kernel_initializer='RandomNormal',
                             bias_initializer=get_bias_init(),
                             name=name)
    #print(conv.dtype_policy)
    #print('x.dtype: %s' % x.dtype.name)
    #print('conv.dtype: %s' % conv.dtype)

    x = prelu(conv(x), name=name)
    # Normalize
    #x = tfa.layers.InstanceNormalization(axis=3,
    #        center=True, 
    #        scale=True,
    #        beta_initializer="random_uniform",
    #        gamma_initializer="random_uniform")(x)
    return layers.BatchNormalization(name=name+'_b_norm')(x)

def conc(x, y, name):
    return layers.concatenate([x, y], name=name)

def score_layer(x, channels, name="score_"):
    "The final layer."
    if channels > 1:
        act = 'softmax'
    else:
        act = 'sigmoid'
    conv = layers.Conv2D(channels, kernel_size=1,
                             activation=act,
                             dtype='float32',
                             padding='same',
                             kernel_regularizer=regul(),
                             kernel_initializer='RandomNormal',
                             bias_initializer=get_bias_init(),
                             name=name+act)
    #print('Outputs dtype: %s' % conv.dtype)
    return conv(x)

# Regularization
def regul():
    if weight_decay:
        return regularizers.l2(weight_decay)
    else:
        return None

def get_bias_init():
    return initializers.Constant(0.1)

# Parametric RELU activiation
def prelu(x, name=None):
    p = layers.PReLU(shared_axes=[1,2], name=name+"/PReLU", 
                     alpha_initializer=initializers.Constant(0.01))
    return p(x)
