Folders & Files
===============
Folders containing the software - packages used by the various scripts.

They are organized as follows:

knet
----
Modules defining a tensorflow.keras implementation of the neural network

net
---
Jacob Madsen's original Tensorflow implementation.
Contains the raw TensorFlow implementation of the code 
(which is not used but kept for reference as it is 
the basis for [JAMAD1]_).

data
----
Modules for handling simulated HRTEM data, and for generating training data.
Data structures for handing and simulated TEM images.

dataset.py
  Defines objects for handling simulated TEM dataset and for
  operations on it.  This file defines the ``DataEntry`` and
  ``DataSet`` classes.

mods.py and labels.py
  Operations modifying the simulated images and generating the
  ground truth images are delegated to this file.  Do not use
  directly, call the methods of the ``DataEntry`` objects.

imagesimul
----------
High-end interface to the image simulation as used by the
training and testing scripts.
Helper objects for generating simulated images.

makeimages.py
  Class for making simulated images in parallel on a multi-core
  machine.  Also works on a single core.

evaluatepeaks.py
  Peak finding and calculations of precision/recall from peak positions.

analysis
--------
Helper modules mainly for plotting and the like.  Obsolete

loaddata.py
-----------
Small helper functions for loading training data, and neural
network weights.
